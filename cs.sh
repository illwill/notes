#!/bin/bash
# Cobalt Strike script to download, unpack, and update
LICENSE=$1
if [ -z "${LICENSE}" ]; then
    echo -e '\nMissing License number!\nPlease input the Cobalt Strike license number in this format: XXXX-XXXX-XXXX-XXXX\n'
    echo -e 'Example1: ./cs.sh XXXX-XXXX-XXXX-XXXX'
    echo -e 'Example2: curl -s https://gitlab.com/illwill/notes/-/raw/main/cs.sh | bash -s xxxx-xxxx-xxxx-xxxx\n'
    exit
fi

echo '_______  _______  _______  _______  ___      _______    _______  _______  ______    ___   ___   _  _______ '
echo '|       ||       ||  _    ||   _   ||   |    |       |  |       ||       ||    _ |  |   | |   | | ||       |'
echo '|       ||   _   || |_|   ||  |_|  ||   |    |_     _|  |  _____||_     _||   | ||  |   | |   |_| ||    ___|'
echo '|       ||  | |  ||       ||       ||   |      |   |    | |_____   |   |  |   |_||_ |   | |      _||   |___ '
echo '|      _||  |_|  ||  _   | |       ||   |___   |   |    |_____  |  |   |  |    __  ||   | |     |_ |    ___|'
echo '|     |_ |       || |_|   ||   _   ||       |  |   |     _____| |  |   |  |   |  | ||   | |    _  ||   |___ '
echo '|_______||_______||_______||__| |__||_______|  |___|    |_______|  |___|  |___|  |_||___| |___| |_||_______|'
echo -e '                               -=[ Automated Installer v0.1 - by illwill ]=-\n'

echo -e "Downloading Cobalt Strike using license#:$LICENSE\n"
PAGE=$(curl -s https://download.cobaltstrike.com/download -d 'dlkey='"$LICENSE"'' | grep 'href=\"/downloads/')
VERSION=$(echo $PAGE | grep 'href=\"/downloads/' | cut -d '/' -f4)
TOKEN=$(echo $PAGE | grep 'href=\"/downloads/' | cut -d '/' -f3)
echo "Our session token is: $TOKEN"
echo "Downloading version: $VERSION"
curl -s https://download.cobaltstrike.com/downloads/$TOKEN/$VERSION/cobaltstrike-dist.tgz -d 'dlkey=$LICENSE' -o cobaltstrike-dist.tgz
echo "Extracting and updating..."
tar -xf cobaltstrike-dist.tgz
cd cobaltstrike
echo $LICENSE | ./update
