# pip3 install shodan art pandas json
import shodan
import argparse
import concurrent.futures
import json
import socket
import requests
import ipaddress
import os
import pandas as pd
import art

API_KEY = 'APIKEYHERE'  # dont forget your API key


def display_ascii_art():
    shodigger_art = art.text2art("  Sho'Nuff", chr_ignore=True)
    blue_text = f"\033[34m{shodigger_art}\033[0m"
    print(blue_text)
    print("  ├── Shodan query parser")
    print("  │   └── \033[96mby illwill\033[0m")
display_ascii_art()

def get_cloudflare_ip_ranges():
    try:
        response = requests.get('https://www.cloudflare.com/ips-v4')
        response.raise_for_status()
        return [ipaddress.ip_network(ip.strip()) for ip in response.text.strip().split('\n')]
    except requests.RequestException as e:
        print(f'Error: {e}')
        return []

def is_cloudflare_ip(ip, cloudflare_ip_ranges):
    ip_obj = ipaddress.ip_address(ip)
    return any(ip_obj in ip_range for ip_range in cloudflare_ip_ranges)

def resolve_hostname(hostname):
    try:
        return socket.gethostbyname(hostname)
    except socket.gaierror:
        print(f'Error: Unable to resolve hostname {hostname}')
        return None

def lookup_target(api, target, cloudflare_ip_ranges):
    result = {"Open_Ports": [], "Services_Info": []}
    messages = []  # Initialize a list to collect messages
    messages.append(f'  ├── Querying: {target}')  # Add the target being queried to messages
    processed_ports = set()  # Track processed ports to avoid duplicates

    try:
        if '/' in target:  # CIDR range
            messages.append(f'  │   ├── CIDR Range: {target}')
            # Add logic here if you want to handle CIDR ranges
        elif target.replace('.', '').isnumeric():  # If it's an IP address
            messages.append(f'  │   ├── IP Address: {target}')
            try:
                host_details = api.host(target)
                if 'ports' in host_details:
                    result['Open_Ports'] = host_details['ports']
                    messages.append(f'  │   ├── Open Ports: {", ".join(map(str, host_details["ports"]))}')
                
                # Assuming API response includes service details in host_details['data']
                if 'data' in host_details:
                    for service in host_details['data']:
                        port = service.get('port')
                        if port and port not in processed_ports:
                            processed_ports.add(port)
                            service_info = {
                                "Port": port,
                                "Service": service.get("product", "Unknown"),
                                "Version": service.get("version", "N/A"),
                                "OS": service.get("os", "N/A")
                            }
                            result["Services_Info"].append(service_info)
                            # Construct message including OS and software (version)
                            messages.append(f'  │   │   └── Port: {port}, Service: {service_info["Service"]}, Version: {service_info["Version"]}, OS: {service_info["OS"]}')

            except shodan.APIError:
                messages.append(f'  │   └── No information available for IP {target} on Shodan.')
        else:  # If it's a hostname
            messages.append(f'  │   ├── Hostname: {target}')
            try:
                search_result = api.search(f"hostname:{target}")
                if search_result.get('matches'):
                    unique_ports = {match.get('port') for match in search_result['matches'] if match.get('port')}
                    result['Open_Ports'] = list(unique_ports)
                    if unique_ports:
                        messages.append(f'  │   ├── Open Ports: {", ".join(map(str, sorted(unique_ports)))}')
                    
                    for match in search_result['matches']:
                        port = match.get('port')
                        if port and port not in processed_ports:
                            processed_ports.add(port)
                            service_info = {
                                "Port": port,
                                "Service": match.get("product", "Unknown"),
                                "Version": match.get("version", "N/A"),
                                "OS": match.get("os", "N/A")
                            }
                            result["Services_Info"].append(service_info)
                            messages.append(f'  │   │   └── Port: {port}, Service: {service_info["Service"]}, Version: {service_info["Version"]}, OS: {service_info["OS"]}')
                else:
                    messages.append(f'  │   └── No detailed info found for {target} on Shodan.')
            except shodan.APIError as e:
                messages.append(f'    │   └── Shodan API error: {e}')
    except Exception as e:
        messages.append(f'  │   └── General Error: {e}')

    return target, result, messages






def extract_services_info_from_matches(matches):
    # Adjust this function to extract info from the 'matches' structure returned by api.search
    services_info = []
    for match in matches:
        # Here you'd extract the relevant info from each match
        # For demonstration, let's assume you're extracting a simplified structure
        service_info = {
            'Port': match.get('port'),
            'Service': match.get('product'),
            'Version': match.get('version'),
            'OS': match.get('os'),
        }
        services_info.append(service_info)
    return services_info


def extract_services_info(host_details):
    services_info = []
    for service in host_details.get('data', []):
        service_info = {
            'Port': service.get('port'),
            'Service': service.get('product'),
            'Version': service.get('version'),
            'OS': service.get('os'),
        }
        services_info.append(service_info)
    return services_info

def format_services_info_messages(services_info):
    messages = []
    for service in services_info:
        # Start with the basic message format including the service and port
        message = f"  │       └── Service on Port {service['Port']}: {service.get('Service', 'Unknown')}"
        
        # Append version information if available
        if 'Version' in service and service['Version']:
            message += f", Version: {service['Version']}"
        
        # Append OS information if available
        if 'OS' in service and service['OS']:
            message += f", OS: {service['OS']}"
        
        messages.append(message)
    return messages




def get_query_credits(api):
    return api.info()['query_credits']



def write_output(results, filename, format_type):
    if format_type == 'json':
        with open(filename, 'w') as file:
            json.dump([{'Target': target, **data} for target, data in results], file, indent=2)
    elif format_type == 'xlsx':
        data_records = []
        for target, data in results:
            for service_info in data.get('Services_Info', []):
                record = {
                    "Target": target,
                    "Port": service_info.get('Port'),
                    "Service": service_info.get('Service', 'Unknown'),
                    "Version": service_info.get('Version', 'N/A'),
                    "OS": service_info.get('OS', 'N/A'),
                    "Open_Ports": ", ".join(map(str, data.get('Open_Ports', [])))
                }
                data_records.append(record)
            if not data.get('Services_Info'):  # If no service info, still add a record for the target
                data_records.append({"Target": target, "Open_Ports": ", ".join(map(str, data.get('Open_Ports', [])))})
        
        df = pd.DataFrame(data_records)
        df.to_excel(filename, index=False)


def main():
    parser = argparse.ArgumentParser(description='Process a single HOST/IP or a file of HOSTs, IPs, CIDR targets for Shodan lookup.')
    parser.add_argument('-t', '--target', help='A single target (HOST or IP).')
    parser.add_argument('-f', '--file', help='The name of the file containing the list of targets.')
    parser.add_argument('-o', '--output', help='Output format (json or xlsx).', choices=['json', 'xlsx'])
    
    args = parser.parse_args()

    api = shodan.Shodan(API_KEY)
    cloudflare_ip_ranges = get_cloudflare_ip_ranges()
    
    targets = []
    if args.target:
        targets.append(args.target)
    elif args.file:
        with open(args.file, 'r') as file:
            targets = [line.strip() for line in file]

    if not targets:
        print("  ├── No target specified. Please provide either a single target with -t or a list of targets with -f.")
        print("  └── Optionally, use -o to specify the output format as either json or xlsx.")
        print("\n Examples:")
        print("    ├── To query a single target and output to screen only:")
        print("    │   └── \033[96mpython shonuff.py -t 192.0.2.1\033[0m")
        print("    ├── To query a single target and output to an Excel file:")
        print("    │   └── \033[96mpython shonuff.py -t example.com -o xlsx\033[0m")
        print("    ├── To process a list of targets from a file and output to JSON:")
        print("    │   └── \033[96mpython shonuff.py -f targets.txt -o json\033[0m")
        print("    └── To process a list of targets from a file and output to an Excel file:")
        print("        └── \033[96mpython shonuff.py -f targets.txt -o xlsx\033[0m")
        return  # Correctly placed inside the main function

    results = []  # Initialize an empty list to store the results
    with concurrent.futures.ThreadPoolExecutor() as executor:
        future_to_target = {executor.submit(lookup_target, api, target, cloudflare_ip_ranges): target for target in targets}
        for future in concurrent.futures.as_completed(future_to_target):
            try:
                target, result, messages = future.result()  # Unpack the result and messages
                results.append((target, result))  # Append the result to the results list
                for message in messages:  # Print the messages
                    print(message)
            except Exception as exc:
                print(f'{future_to_target[future]} generated an exception: {exc}')

    basename = "output"  # Default output file name
    if args.file:
        basename = os.path.splitext(os.path.basename(args.file))[0]
    output_filename = f'{basename}.{args.output}'
    write_output(results, output_filename, args.output)  # Pass the collected results to write_output
    
    final_credits = get_query_credits(api)
    print(f'  └── Your Query Credits: {final_credits}\n')

if __name__ == '__main__':
    main()
