# pentesting
Collection of notes and scripts

<dl>
<dt>cs.sh</dt>
<dd>BASH script to automate download,installing, and updating Cobalt Strike after inputting your current license</dd>


- **locally:**  
`chmod +x ./cs.sh & ./cs.sh XXXX-XXXX-XXXX-XXXX`    
- **remotely:**  
`curl -s https://gitlab.com/illwill/notes/-/raw/main/cs.sh | bash -s xxxx-xxxx-xxxx-xxxx`

<dt>bucketlist.sh</dt>
<dd>BASH script to check AWS S3 buckets for a possible takeover</dd>

<dt>certs.sh</dt>
<dd>BASH script to passively find subdomains</dd>
<dt>HTTPsC2DoneRight.sh</dt>
<dd>BASH script for HTTPS C2 Done Right Setup Script (not mine)</dd> 
<dt>sho'nuff.py</dt>
<dd>Python script to process a file of HOST, IP, CIDR targets for Shodan lookup.</dd>
</dl>

