#!/bin/bash
#####################################################
# Passively collects subdomains without bruteforcing
# using returned data in SSL/TLS certificates
# Usage: certs.sh [domain.name|domains.txt]
#####################################################

banner() {
    printf "\e[33m"
    printf "      ▄▄· ▄▄▄ .▄▄▄  ▄▄▄▄▄.▄▄ · \n"
    printf "     ▐█ ▌▪▀▄.▀·▀▄ █·•██  ▐█ ▀.\n"
    printf "     ██ ▄▄▐▀▀▪▄▐▀▀▄  ▐█.▪▄▀▀▀█▄\n"
    printf "     ▐███▌▐█▄▄▌▐█•█▌ ▐█▌·▐█▄▪▐█\n"
    printf "     ·▀▀▀  ▀▀▀ .▀  ▀ ▀▀▀  ▀▀▀▀ \e[0m\n"
    printf "\e[92m-=[Passive Sub-Domain Enumeration]=-\n\e[0m"
    printf "\e[92m          -=[by illwill]=-           \n\e[0m"
}
banner  # show the banner

# Check if jq is installed
if ! command -v jq &> /dev/null; then
    printf "\033[31mERROR:\033[0m jq is not installed. Please install it and try again.\n"
    printf "\033[93mFor Debian/Ubuntu:\033[0m sudo apt-get install jq\n"
    printf "\033[93mFor RedHat/CentOS:\033[0m sudo yum install jq\n"
    printf "\033[93mFor macOS:\033[0m brew install jq\n"
    exit 1
fi

temp_file=$(mktemp)
process_domain() {
    local url="$1"

    printf "├─── [\e[92m*\e[0m] GATHERING SUBDOMAINS FROM: $url\n"

    enum_crtsh() {
        curl -s "https://crt.sh/?q=%25.$url" | sed 's/<\/\?[^>]\+>/\n/g' | sort -u | grep -v "LIKE" | grep -v "crt.sh" | grep "$url" | sed 's/ //' | grep -v "*" | grep "$url"
    }
    crtsh=$(enum_crtsh "$url")

    enum_certspotter() {
        curl -s "https://api.certspotter.com/v1/issuances?domain=$url&include_subdomains=true&expand=dns_names" | jq .[].dns_names | grep -Po "(([\w.-]*)\.([\w]*)\.([A-z]))\w+" | sort -u
    }
    certspotter=$(enum_certspotter "$url")

    enum_hackertarget() {
        curl -s "https://api.hackertarget.com/hostsearch/?q=$url" | cut -d',' -f1 | sort -u
    }
    hackertarget=$(enum_hackertarget "$url")

    enum_alienvault() {
        curl -s "https://otx.alienvault.com/api/v1/indicators/domain/$url/url_list?limit=100&page=1" | grep -o '"hostname": *"[^"]*' | sed 's/"hostname": "//' | sort -u
    }
    alienvault=$(enum_alienvault "$url")

    # Append results to the temporary file
    printf "%s\n" "$crtsh" "$certspotter" "$hackertarget" "$alienvault" | tr -s " " "\n" | tr '[:upper:]' '[:lower:]' >> "$temp_file"
}

# Check if a domain name or file was entered
if [ -z "$1" ]; then
    printf "[\033[31mx\e[0m]\033[31m ERROR:\e[0m Need a domain name or file\n" >&2
    printf "└──\e[93mUsage:\e[0m $0 [domain.name|domains.txt]\n"
    exit 1
fi

# Check if the input is a file or a single domain
if [ -f "$1" ]; then
    # If it's a file, read each line as a domain name
    while IFS= read -r domain; do
        if [ ! -z "$domain" ]; then
            process_domain "$domain"
        fi
    done < "$1"
else
    # If it's not a file, assume it's a single domain
    process_domain "$1"
fi

# Sort and unique the results, and output to subdomains.txt
grep '\.' "$temp_file" | sort -u -o subdomains.txt

# Clean up the temporary file
printf "└─┬ [\e[92m?\e[0m] Found \e[93m%s\e[0m subdomains\n" "$(wc -l < subdomains.txt)"
printf "  └─── [\e[92m+\e[0m] Complete. All results in \e[93msubdomains.txt\e[0m\n"
rm "$temp_file"