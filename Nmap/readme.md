#### External discovery scan
```
nmap --stats-every 60s --log-errors --traceroute --reason --randomize-hosts -v -R -PE -PP -PM -PO -PU -PY80,23,443,21,22,25,3389,110,445,139 -PS80,23,443,21,22,25,3389,110,445,139,143,53,135,3306,8080,1723,111,995,993,5900,1025,587,8888,199,1720,465,548,113,81,6001,10000,514,5060,179,1026,2000,8443,8000,32768,554,26,1433,49152,2001,515,8008,49154,1027,5666,646 -PA80,23,443,21,22,25,3389,110,445,139,143,53,135,3306,8080,1723,111,995,993,5900,1025,587,8888,199,1720,465,548,113,81,6001,10000,514,5060,179,1026,2000,8443,8000,32768,554,26,1433,49152,2001,515,8008,49154,1027,5666,646 -sS -sV -p21,22,23,25,80,443,8080,8443 -iL xxx -oA xxx
```
#### Quick discovery scan
```
nmap --stats-every 60s --log-errors --traceroute --reason --randomize-hosts -v -R -PE -PM -PO -PU -PS80,23,443,21,22,25,3389,110,445,139 -PA80,443,22,445,139 -sS -sV -p21,22,23,25,80,443,8080,8443 -iL xxx -oA xxx
```

#### Grep live hosts from discovery scans
```
grep -i "status: up" <disco>.gnmap | cut -d " " -f 2 > live_hosts.txt
```

#### Windows Scan
```
sudo nmap -oA <filename> --stats-every 60s --log-errors --reason --randomize-hosts -v -R -PE -PM -PO -PU -PS80,23,443,21,22,25,3389,110,445,139 -PA80,443,22,445,139 -sS -sV -p21,22,23,25,80,443,8080,8443 <IP Range> or -iL <Host file>
``` 
#### Full TCP
```
sudo nmap -oA <filename> --stats-every 60s --log-errors --reason --randomize-hosts -v -R -Pn -n -p- -iL <Host file>
 ```
#### UDP top 100
```
sudo nmap -oA <filename> --stats-every 60s --log-errors --reason --randomize-hosts -v -R -Pn -n -sUVC -A --top-ports 100 -iL <Host file>
```

#### Fast Reverse DNS Scan
```
nmap -sL -iL <ranges.txt>
```

#### Create IP Host list
```
nmap -sL -n -iL <ranges.txt> | cut -d " " -f5 > ips.txt
```
