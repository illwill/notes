#!/bin/bash
banner() {
    echo -e "\e[34m"
    echo -e "  _                _        _   _ _     _ "
    echo -e " | |              | |      | | | (_)   | |"
    echo -e " | |__  _   _  ___| | _____| |_| |_ ___| |_"
    echo -e " | '_ \| | | |/ __| |/ / _ \ __| | / __| __|"
    echo -e " | |_) | |_| | (__|   <  __/ |_| | \\__ \\ |_ "
    echo -e " |_.__/ \\__,_|\___|_|\\_\\___|\\__|_|_|___/\\__|\e[0m"
    echo -e "\e[33m               _______"
    echo -e "\e[90m          _.-\e[33m ()______)\e[90m-._           \e[34m,"
    echo -e "\e[90m         .'               '.        \e[34m/ \\"
    echo -e "\e[90m        /                   \\      \e[34m(   )"
    echo -e "\e[90m       :      _________      :      \e[34m\"-\""
    echo -e "\e[90m       |.--'''         '''--.|"
    echo -e "       (                     )"
    echo -e "       :'--..._________...--':"
    echo -e "       :                     :"
    echo -e "        :                   :"
    echo -e "        :                   :"
    echo -e "         :                 :"
    echo -e "         :                 :"
    echo -e "          :               :"
    echo -e "          :               :"
    echo -e "           :             :"
    echo -e "           :_           _:"
    echo -e "             '''-----'''"
    echo -e "\e[92m     -=[Bucket Takeover Tester]=-\e[0m"
    echo -e "\e[92m           -=[by illwill]=-\n\e[0m"
}
banner  # show the banner

check_and_install_httpie() {
    # Check if httpie is installed
    if ! command -v http &> /dev/null
    then
        echo "httpie could not be found, installing..."
        # Update package list and Install httpie
        sudo apt-get update && sudo apt-get install httpie -y
        # Check if the installation was successful
        if command -v http &> /dev/null
        then
            echo "httpie installed successfully."
        else
            echo "Failed to install httpie. Please install it manually."
        fi
    fi
}
check_and_install_httpie

if [[ $# -eq 0 ]]; then
    echo "Usage: $0 <file-name>"
    exit 1
fi

FILE_NAME=$1

if [[ ! -f $FILE_NAME ]]; then
    echo "File not found: $FILE_NAME"
    exit 1
fi
echo -n "" > possible.txt
process_url() {
    URL=$1
    http -b GET "$URL" --verify no 2>/dev/null | grep -E -q '<Code>NoSuchBucket</Code>|<li>Code: NoSuchBucket</li>' \
    && { 
        echo -e "├──[\e[92m?\e[0m] Subdomain takeover \e[32mmay be possible\e[0m for $URL"
        echo "$URL" >> possible.txt
    } \
    || echo -e "├──[\e[31mx\e[0m] Subdomain takeover \e[31mis not possible\e[0m for $URL"
}


export -f process_url
cat $FILE_NAME | xargs -P 10 -I {} bash -c "process_url {}"
echo -e "└─┬[\e[92m?\e[0m] Complete.\n  └─── [\e[92m+\e[0m] Total possible URLs in possible.txt: $(wc -l < possible.txt)\n"
cat possible.txt
